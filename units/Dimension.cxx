#include "units/Dimension.h"
#include "units/System.h"

#include <iostream>
#include <sstream>

units_BEGIN_NAMESPACE

InternalDimension::InternalDimension(units::string::Token symbol, units::string::Token name, units::string::Token description)
  : Entity(symbol, name, description)
{
}

InternalDimension::InternalDimension(
  units::string::Token symbol,
  units::string::Token name,
  units::string::Token plural,
  units::string::Token description)
  : Entity(symbol, name, plural, description)
{
}

CompositeDimension::CompositeDimension(const CompositeDimension& other)
  : InternalDimension(other.m_symbol, other.m_name, other.m_plural, other.m_description)
  // TODO: Handle aliases in Entity as well
  , m_basis(other.m_basis)
{
}

CompositeDimension::CompositeDimension(units::string::Token symbol, units::string::Token name, units::string::Token description)
  : InternalDimension(symbol, name, description)
{
}

CompositeDimension::CompositeDimension(
  units::string::Token symbol, units::string::Token name, const InternalDimension::Exponents& basis, units::string::Token description)
  : InternalDimension(symbol, name, description)
  , m_basis(basis)
{
}

CompositeDimension::CompositeDimension(
  units::string::Token symbol, units::string::Token name, const CompositeDimension& basis, units::string::Token description)
  : InternalDimension(symbol, name, description)
  , m_basis(basis.m_basis)
{
}

CompositeDimension::CompositeDimension(
  units::string::Token symbol, units::string::Token name, units::string::Token plural, units::string::Token description)
  : InternalDimension(symbol, name, plural, description)
{
}

CompositeDimension::CompositeDimension(
  units::string::Token symbol,
  units::string::Token name,
  units::string::Token plural,
  const InternalDimension::Exponents& basis,
  units::string::Token description)
  : InternalDimension(symbol, name, plural, description)
  , m_basis(basis)
{
}

CompositeDimension::CompositeDimension(
  units::string::Token symbol,
  units::string::Token name,
  units::string::Token plural,
  const CompositeDimension& basis,
  units::string::Token description)
  : InternalDimension(symbol, name, plural, description)
  , m_basis(basis.m_basis)
{
}

CompositeDimension& CompositeDimension::operator = (const CompositeDimension& other)
{
  m_name = other.m_name;
  m_plural = other.m_plural;
  m_symbol = other.m_symbol;
  m_description = other.m_description;
  m_nameAliases = other.m_nameAliases;
  m_symbolAliases = other.m_symbolAliases;
  m_basis = other.m_basis;
  return *this;
}

Dimension Dimension::pow(double exponent) const
{
  Dimension result;
  if (exponent == 0.)
  {
    return result; // Raising to the 0-th power removes all dimensions.
  }

  result = *this;
  // Multiply each exponent.
  for (auto& entry : result.m_data)
  {
    entry.second *= exponent;
  }
  return result;
}

System* Dimension::system() const
{
  if (m_data.empty())
  {
    return nullptr;
  }
  return m_data.begin()->first->system();
}

units::string::Token Dimension::name() const
{
  using namespace units::string::literals;

  // If the dimension is not a composite of multiple dimensions
  // and has an exponent of 1, just return the InternalDimension's name:
  if (m_data.size() == 1 && m_data.begin()->second == 1.0)
  {
    return m_data.begin()->first->m_name;
  }

  // If the Dimension has no entries in its map of (InternalDimension, exponent) tuples,
  // then return "none":
  auto sys = this->system();
  if (!sys)
  {
    return "none";
  }

  // Look in the unit system for a named composite dimension.
  auto it = sys->m_compositeDimensions.find(*this);
  if (it == sys->m_compositeDimensions.end())
  {
    // There is no named composite dimension, so return the canonical
    // stringified set of dimensions involved.
    std::ostringstream unnamed;
    unnamed << *this;
    return unnamed.str();
  }

  // Return the named composite dimension name.
  return it->second->m_name;
}

std::string Dimension::html(bool principleDimensionsOnly) const
{
  using namespace units::string::literals;

  // If the dimension is not a composite of multiple dimensions
  // and has an exponent of 1, just return the InternalDimension's symbol:
  if (m_data.size() == 1 && m_data.begin()->second == 1.0)
  {
    return m_data.begin()->first->m_symbol.valid() ?
      m_data.begin()->first->m_symbol.data() :
      m_data.begin()->first->m_name.data();
  }

  // If we have a unit system and are asked to report
  // composite dimensions, check to see if this dimension
  // is a named composite dimension.
  auto sys = this->system();
  if (!principleDimensionsOnly && sys)
  {
    // Look in the unit system for a named composite dimension.
    auto it = sys->m_compositeDimensions.find(*this);
    if (it != sys->m_compositeDimensions.end())
    {
      // Return the named composite dimension name.
      return it->second->m_symbol.valid() ?
        it->second->m_symbol.data() :
        it->second->m_name.data();
    }
  }

  // There is no named composite dimension (or we were told not
  // to use it), so return the canonical stringified set of
  // dimensions involved.
  std::ostringstream result;
  bool once = false;
  for (const auto& entry : m_data)
  {
    if (!entry.first) { continue; }
    if (once)
    {
      result << "·";
    }
    else
    {
      once = true;
    }
    if (entry.first->m_symbol.valid() && !entry.first->m_symbol.data().empty())
    {
      result << entry.first->m_symbol.data();
    }
    else
    {
      // If the symbol is invalid, the name must be valid.
      result << entry.first->m_name.data();
    }
    if (entry.second != 1.0)
    {
      result << "<sup>" << entry.second << "</sup>";
    }
  }
  if (result.str().empty())
  {
    result << "∅";
  }
  return result.str();
}

void Dimension::dump() const
{
  std::cout << *this << "\n";
}

Dimension operator * (const Dimension& a, const Dimension& b)
{
  Dimension result;
  for (const auto& entry : a.m_data)
  {
    if (const auto* da = dynamic_cast<const CompositeDimension*>(entry.first))
    {
      for (const auto& innerEntry : da->m_basis)
      {
        result.m_data[innerEntry.first] += innerEntry.second * entry.second;
      }
    }
    else
    {
      result.m_data[const_cast<InternalDimension*>(entry.first)] += entry.second;
    }
  }
  for (const auto& entry: b.m_data)
  {
    if (const auto* db = dynamic_cast<const CompositeDimension*>(entry.first))
    {
      for (const auto& innerEntry : db->m_basis)
      {
        result.m_data[innerEntry.first] += innerEntry.second * entry.second;
      }
    }
    else
    {
      result.m_data[const_cast<InternalDimension*>(entry.first)] += entry.second;
    }
  }
  // Prune keys with zero values:
  for (auto it = result.m_data.begin(); it != result.m_data.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      result.m_data.erase(prev);
    }
  }
  return result;
}

Dimension operator / (const Dimension& aa, const Dimension& bb)
{
  Dimension result;
  // Add to the "numerator"
  for (const auto& entry : aa.m_data)
  {
    if (const auto* da = dynamic_cast<const CompositeDimension*>(entry.first))
    {
      for (const auto& innerEntry : da->m_basis)
      {
        result.m_data[innerEntry.first] += innerEntry.second * entry.second;
      }
    }
    else
    {
      result.m_data[const_cast<InternalDimension*>(entry.first)] += entry.second;
    }
  }
  // Subtract from the "denominator"
  for (const auto& entry: bb.m_data)
  {
    if (const auto* db = dynamic_cast<const CompositeDimension*>(entry.first))
    {
      for (const auto& innerEntry : db->m_basis)
      {
        result.m_data[innerEntry.first] -= innerEntry.second * entry.second;
      }
    }
    else
    {
      result.m_data[const_cast<InternalDimension*>(entry.first)] -= entry.second;
    }
  }
  // Prune keys with zero values:
  for (auto it = result.m_data.begin(); it != result.m_data.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      result.m_data.erase(prev);
    }
  }
  return result;
}

bool operator == (const Dimension& aa, const Dimension& bb)
{
  return aa.m_data == bb.m_data;
}

bool operator != (const Dimension& aa, const Dimension& bb)
{
  return aa.m_data != bb.m_data;
}

bool operator < (const Dimension& aa, const Dimension& bb)
{
  return aa.m_data < bb.m_data;
}

units_CLOSE_NAMESPACE
