#ifndef units_Measurement
#define units_Measurement

#include "units/Unit.h"

units_BEGIN_NAMESPACE

/// A value plus the units in which it is measured.
struct UNITS_EXPORT Measurement
{
  /// Construct a default (dimensionless, zero-valued) measurement.
  Measurement() = default;
  /// Construct a measurement with the given \a value and \a units.
  Measurement(double value, Unit units);

  /// For debugging, print this measurement to the console.
  void dump() const;

  double m_value{ 0. };
  Unit m_units;
};

/// Print a measurement to the given stream \a str.
template<typename Stream>
Stream& operator << (Stream& str, const Measurement& mm)
{
  str << mm.m_value;
  if (!mm.m_units.dimensionless())
  {
    if (mm.m_units.m_powers.empty())
    {
      str << " " << mm.m_units;
    }
    else
    {
      str << "×" << mm.m_units;
    }
  }
  return str;
}

units_CLOSE_NAMESPACE

#endif // units_Measurement
