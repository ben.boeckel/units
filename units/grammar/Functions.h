#ifndef units_grammar_Functions_h
#define units_grammar_Functions_h

#include "units/grammar/Action.h"

units_BEGIN_NAMESPACE
namespace grammar
{

bool UNITS_EXPORT parseUnits(const std::string& input, measurement_state& state);

} // namespace grammar
units_CLOSE_NAMESPACE


#endif // units_grammar_Functions_h
