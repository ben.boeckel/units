#include "units/grammar/Functions.h"
#include "units/string/Token.h"

#include "tao/pegtl/contrib/parse_tree.hpp"

namespace
{
// taken from vtk/Common/Core/vtkTypeName.h
std::string fixup(const std::string& name)
{
  std::string result = name;

  // we need to remove
  // MSVC-specific cruft from the symbol name.
#if defined(_MSC_VER)
  // MSVC returns a name with "class " or "struct " prepended. Remove it
  // for consistency with other platforms. Note that template parameters
  // also include "class " or "struct ", so we must search and replace
  // repeatedly.
  for (std::string::size_type pos = result.find("class "); pos != std::string::npos;
       pos = result.find("class ", pos + 1))
  {
    result = result.substr(0, pos) + result.substr(pos + 6);
  }
  for (std::string::size_type pos = result.find("struct "); pos != std::string::npos;
       pos = result.find("struct ", pos + 1))
  {
    result = result.substr(0, pos) + result.substr(pos + 7);
  }
  // MSVC reports anonymous namespaces like so: `anonymous namespace'
  // while others report them like so: (anonymous namespace). Fix it
  // to be consistent.
  for (std::string::size_type pos = result.find("`anonymous namespace'");
       pos != std::string::npos; pos = result.find("`anonymous namespace'", pos + 1))
  {
    result = result.substr(0, pos) + "(anonymous namespace)" + result.substr(pos + 21);
  }
  // MSVC does not include spaces after commas separating template
  // parameters. Add it in:
  for (std::string::size_type pos = result.find(','); pos != std::string::npos;
       pos = result.find(',', pos + 1))
  {
    result = result.substr(0, pos) + ", " + result.substr(pos + 1);
  }
#endif
  return result;
}
}

units_BEGIN_NAMESPACE
namespace grammar
{

using namespace string::literals;

// ----- dimension processing

template<typename NodeType>
void recurse_basic_dimension(NodeType* node, measurement_state& state)
{
  std::string text = node->content();
  ::units::string::Token token(text);
  auto it = state.system->m_dimensions.find(token);
  if (it == state.system->m_dimensions.end())
  {
    throw tao::pegtl::parse_error("Unknown dimension \"" + text + "\".", node->begin());
  }
  state.dimensions = Dimension({*it->second, 1});
}

template<typename NodeType>
void recurse_power_dimension(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  tmp.parsing_dimensions = true;
  recurse_basic_dimension(node->children.front().get(), tmp);
  if (node->children.size() == 1)
  {
    state.dimensions = tmp.dimensions;
    return;
  }
  // TODO: This ignores the work done by capture_exponent…
  double exponent = strtod(node->children.back()->content().c_str(), nullptr);
  state.dimensions = tmp.dimensions.pow(exponent);
}

template<typename NodeType>
void recurse_product_dimension(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  tmp.parsing_dimensions = true;
  bool numer = true;
  for (const auto& child : node->children)
  {
    ::units::string::Token childType(fixup(child->name()));
    switch (childType.id())
    {
    case "units::grammar::divide_kw"_hash:
      numer = false;
      break;
    case "units::grammar::multiply_kw"_hash:
      numer = true;
      break;
    case "units::grammar::lparen_kw"_hash:
    case "units::grammar::rparen_kw"_hash:
      // Ignore these; they must always be at the
      // begin (respectively, end) of the children:
      // assert(child == node->children.front() || child == node->children.back())
      // They occur because we evaluate a parenthetical expression below
      // as if it were a sequence of product expressions (which it is).
      break;
    case "units::grammar::paren_expression"_hash:
      tmp.dimensions = Dimension();
      recurse_product_dimension(child.get(), tmp);
      state.dimensions = state.dimensions * (numer ? tmp.dimensions : tmp.dimensions.pow(-1));
      break;
    case "units::grammar::power_expression"_hash:
      tmp.dimensions = Dimension();
      recurse_power_dimension(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.dimensions = state.dimensions * (numer ? tmp.dimensions : tmp.dimensions.pow(-1));
      break;
    case "units::grammar::product_expression"_hash:
      tmp.dimensions = Dimension();
      recurse_product_dimension(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.dimensions = state.dimensions * (numer ? tmp.dimensions : tmp.dimensions.pow(-1));
      break;
    default:
    case "units::grammar::foo"_hash:
      std::cerr << "Unsupported product child node " << child->name() << ".";
      break;
    }
  }
}

template<typename NodeType>
void recurse_shift_dimension(NodeType* node, measurement_state& state)
{
  if (node->children.size() != 1)
  {
    throw tao::pegtl::parse_error("Dimension expressions cannot apply reference shifts.", node->begin());
  }
  recurse_product_dimension(node->children.front().get(), state);
}

template<typename NodeType>
void recurse_tree_dimension(NodeType* node, measurement_state& state)
{
  if (!node) { return; }
  ::units::string::Token nodeType(fixup(node->name()));
  switch (nodeType.id())
  {
  case "units::grammar::product_expression"_hash:
    // children should be multiplied together, paying attention to any divide_kw tokens.
    recurse_product_dimension(node, state);
    break;
  case "units::grammar::power_expression"_hash:
    // child is a unit raised to a power.
    recurse_power_dimension(node, state);
    break;
  case "units::grammar::shifted_expression"_hash:
    // child is either a subexpression or a shift expression.
    // If it is a shift expression, there will be multiple children.
    if (node->children.size() == 1)
    {
      recurse_tree_dimension(node->children.front().get(), state);
    }
    else
    {
      recurse_shift_dimension(node, state);
    }
    break;
  case "units::grammar::basic_expression"_hash:
    // child is a single unit.
    recurse_basic_dimension(node, state);
    break;
  }
}

// ----- unit processing

template<typename NodeType>
void recurse_basic(NodeType* node, measurement_state& state)
{
  std::string text = node->content();
  ::units::string::Token token(text);
  Prefix prefix;
  auto it = state.system->m_unitsByName.find(token);
  if (it == state.system->m_unitsByName.end())
  {
    bool found = false;
    // No unit-only match. See if we can find a prefix matching the start of the string.
    for (const auto& prefixEntry : state.system->m_prefixes)
    {
      std::string pfx = prefixEntry.first.data();
      string::Token testPrefix = text.substr(0, pfx.size());
      if (testPrefix == prefixEntry.first)
      {
        it = state.system->m_unitsByName.find(text.substr(pfx.size(), std::string::npos));
        if (it != state.system->m_unitsByName.end())
        {
          found = true;
          prefix = *prefixEntry.second;
        }
      }
    }
    if (!found)
    {
      throw tao::pegtl::parse_error("Unknown unit \"" + text + "\".", node->begin());
    }
  }
  Unit unit({*it->second, 1});
  if (prefix.m_prefix)
  {
    unit = prefix * unit;
  }
  state.result = state.result * unit;
}

template<typename NodeType>
void recurse_power(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  recurse_basic(node->children.front().get(), tmp);
  if (node->children.size() == 1)
  {
    state.result = tmp.result;
    return;
  }
  // TODO: This ignores the work done by capture_exponent…
  double exponent = strtod(node->children.back()->content().c_str(), nullptr);
  state.result = tmp.result.pow(exponent);
}

template<typename NodeType>
void recurse_value(NodeType* node, measurement_state& state)
{
  double val = strtod(node->content().c_str(), nullptr);
  if (state.have_value)
  {
    std::cerr << "Sequences of values not supported yet. Ignoring " << node->content() << ".\n";
    return;
  }
  state.value = val;
  state.have_value = true;
}

template<typename NodeType>
void recurse_product(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  bool numer = true;
  for (const auto& child : node->children)
  {
    ::units::string::Token childType(fixup(child->name()));
    switch (childType.id())
    {
    case "units::grammar::divide_kw"_hash:
      numer = false;
      break;
    case "units::grammar::multiply_kw"_hash:
      numer = true;
      break;
    case "units::grammar::lparen_kw"_hash:
    case "units::grammar::rparen_kw"_hash:
      // Ignore these; they must always be at the
      // begin (respectively, end) of the children:
      // assert(child == node->children.front() || child == node->children.back())
      // They occur because we evaluate a parenthetical expression below
      // as if it were a sequence of product expressions (which it is).
      break;
    case "units::grammar::paren_expression"_hash:
      tmp.result = Unit();
      recurse_product(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.value = state.value * (numer ? tmp.value : 1.0 / tmp.value);
      state.result = state.result * (numer ? tmp.result : tmp.result.pow(-1));
      break;
    case "units::grammar::power_expression"_hash:
      tmp.result = Unit();
      recurse_power(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.value = state.value * (numer ? tmp.value : 1.0 / tmp.value);
      state.result = state.result * (numer ? tmp.result : tmp.result.pow(-1));
      break;
    case "units::grammar::product_expression"_hash:
      tmp.result = Unit();
      recurse_product(child.get(), tmp);
      // TODO: Handle prefixes/values properly?
      state.value = state.value * (numer ? tmp.value : 1.0 / tmp.value);
      state.result = state.result * (numer ? tmp.result : tmp.result.pow(-1));
      break;
    default:
    case "units::grammar::foo"_hash:
      std::cerr << "Unsupported product child node " << child->name() << ".";
      break;
    }
  }
}

template<typename NodeType>
void recurse_shift(NodeType* node, measurement_state& state)
{
  measurement_state tmp;
  tmp.system = state.system;
  recurse_product(node->children.front().get(), tmp);
  if (node->children.size() == 1)
  {
    state.result = tmp.result;
    return;
  }
  double shift = strtod(node->children.back()->content().c_str(), nullptr);
  // TODO: FIXME: Need to figure out how to handle shifts.
  state.result = tmp.result;
}

template<typename NodeType>
void recurse_tree(NodeType* node, measurement_state& state)
{
  if (!node) { return; }
  ::units::string::Token nodeType(fixup(node->name()));
  switch (nodeType.id())
  {
  case "units::grammar::value"_hash:
    recurse_value(node, state);
    break;
  case "units::grammar::product_expression"_hash:
    // children should be multiplied together, paying attention to any divide_kw tokens.
    recurse_product(node, state);
    break;
  case "units::grammar::power_expression"_hash:
    // child is a unit raised to a power.
    recurse_power(node, state);
    break;
  case "units::grammar::shifted_expression"_hash:
    // child is either a subexpression or a shift expression.
    // If it is a shift expression, there will be multiple children.
    if (node->children.size() == 1)
    {
      recurse_tree(node->children.front().get(), state);
    }
    else
    {
      recurse_shift(node, state);
    }
    break;
  case "units::grammar::basic_expression"_hash:
    // child is a single unit.
    recurse_basic(node, state);
    break;
  }
}

template<typename NodeType>
void print_tree(NodeType* node)
{
  if (!node) { return; }
  if (node->has_content())
  {
    std::cout << "  n" << node << " [label=\"" << node->name() << ", text " << node->content() << "\"]\n";
  }
  else
  {
    std::cout << "  n" << node << "\n";
  }
  for (const auto& child : node->children)
  {
    print_tree(child.get());
  }
  for (const auto& child : node->children)
  {
    std::cout << "  n" << node << " -> n" << child.get() << "\n";
  }
}

#if 0
template<typename Rule>
using unit_selector = tao::pegtl::parse_tree::selector<
  Rule,
  tao::pegtl::parse_tree::store_content::on<
    divide_kw,
    shift_kw,
    exponent_kw,
    power_expression,
    basic_expression,
    value
  >
>;

#else
template<typename Rule> struct unit_selector : std::false_type {};
template<> struct unit_selector<divide_kw> : std::true_type {};
template<> struct unit_selector<shift_kw> : std::true_type {};
template<> struct unit_selector<multiply_kw> : std::true_type {};
template<> struct unit_selector<lparen_kw> : std::true_type {};
template<> struct unit_selector<rparen_kw> : std::true_type {};
template<> struct unit_selector<exponent_kw> : std::true_type {};
template<> struct unit_selector<exponent> : std::true_type {};
template<> struct unit_selector<shifted_expression> : std::true_type {};
template<> struct unit_selector<product_expression> : std::true_type {};
template<> struct unit_selector<paren_expression> : std::true_type {};
template<> struct unit_selector<power_expression> : std::true_type {};
template<> struct unit_selector<basic_expression> : std::true_type {};
template<> struct unit_selector<value> : std::true_type {};
#endif

bool parseUnits(const std::string& input, measurement_state& state)
{
  bool ok = true;
  tao::pegtl::string_input<> in(input, "parseUnits");
  try
  {
    std::unique_ptr<tao::pegtl::parse_tree::node> forest;
    if (state.parsing_measurement)
    {
      forest = tao::pegtl::parse_tree::parse<measurement, unit_selector>(in);
    }
    else
    {
      forest = tao::pegtl::parse_tree::parse<units, unit_selector>(in);
    }
    state.result = Unit();
    // std::cout << "digraph {\n";
    for (const auto& tree : forest->children)
    {
      // print_tree(tree.get());
      if (state.parsing_dimensions)
      {
        recurse_tree_dimension(tree.get(), state);
      }
      else
      {
        recurse_tree(tree.get(), state);
      }
    }
    // std::cout << "}\n";
  }
  catch (tao::pegtl::parse_error& err)
  {
    const auto p = err.positions.front();
    if (state.m_debug)
    {
      std::cerr << err.what() << "\n"
        << in.line_as_string(p) << "\n"
        << std::string(p.byte_in_line, ' ') << "^\n";
    }
    ok = false;
    return ok;
  }

  return ok;
}

} // namespace grammar
units_CLOSE_NAMESPACE
