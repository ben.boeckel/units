#ifndef units_grammar_Action_h
#define units_grammar_Action_h

#include "units/grammar/Grammar.h"

units_BEGIN_NAMESPACE
namespace grammar
{

template<typename Rule>
struct Action : nothing<Rule>
{
};

struct UNITS_EXPORT capture_value
{
  template<typename Input>
  static void apply(const Input& input, measurement_state& s)
  {
    // std::cout << "value «" << input.string() << "»\n";
    s.have_value = true;
    s.value = std::strtod(input.string().c_str(), nullptr);
  }
};

struct UNITS_EXPORT capture_exponent
{
  template<typename Input>
  static void apply(const Input& input, measurement_state& s)
  {
    s.exponent = std::strtod(input.string().c_str(), nullptr);
  }
};

template<> struct Action<exponent> : capture_exponent {};
template <> struct Action<value> : capture_value {};

} // namespace grammar
units_CLOSE_NAMESPACE

#endif // units_grammar_Action_h
