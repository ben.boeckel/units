#ifndef units_grammar_Grammar_h
#define units_grammar_Grammar_h

#include "units/System.h"

#include "tao/pegtl.hpp"

#include <iostream>

units_BEGIN_NAMESPACE
namespace grammar
{

using namespace tao::pegtl;

struct UNITS_EXPORT measurement_state
{
  bool have_value{ false };
  double value{ 1.0 };
  double exponent{ 1.0 };
  bool have_md{ false };
  bool is_div{ false };
  /// When parsing dimensions (rather than units), set this to true.
  bool parsing_dimensions{ false };
  /// When parsing measurements (a value plus units), set this to true.
  bool parsing_measurement{ false };
  /// Print parse errors to the console.
  bool m_debug{ false };

  std::string units;
  Unit basic;
  std::vector<Unit> power;
  Unit product;
  Unit shifted;
  std::shared_ptr<System> system;
  Unit result;
  Dimension dimensions;
};

// Avoid a dependence on libicu:
struct UNITS_EXPORT white_space :
  sor<
    one<' ', '\t', '\n', '\v', '\f', '\r'>,
    TAO_PEGTL_ISTRING(" "), // non-breaking space, U+00a0
    TAO_PEGTL_ISTRING(" "), // en space, U+2002
    TAO_PEGTL_ISTRING(" "), // em space, U+2003
    TAO_PEGTL_ISTRING(" "), // three-per-em space, U+2004
    TAO_PEGTL_ISTRING(" "), // four-per-em space, U+2005
    TAO_PEGTL_ISTRING(" "), // six-per-em space, U+2006
    TAO_PEGTL_ISTRING(" "), // figure space, U+2007
    TAO_PEGTL_ISTRING(" "), // punctuation space, U+2008
    TAO_PEGTL_ISTRING(" "), // thin space, U+2009
    TAO_PEGTL_ISTRING(" "), // hair space, U+200a
    TAO_PEGTL_ISTRING(" "), // narrow non-breaking space, U+202f
    TAO_PEGTL_ISTRING(" "), // medium mathematical space, U+205f
    TAO_PEGTL_ISTRING("␠"), // symbol for space, U+2420
    TAO_PEGTL_ISTRING("　"), // ideographic space, U+3000
    TAO_PEGTL_ISTRING("𑽈") // Kawi punctation space, U+11f48
    // TAO_PEGTL_ISTRING(" "), // Ogham space mark, U+1680
  > {};

// -----
// This fragment (floating-point parse rules) has been
// adapted from PEGTL's example double:
// Copyright (c) 2014-2018 Dr. Colin Hirsch and Daniel Frey
// Visit https://github.com/taocpp/PEGTL/ for license information.
struct UNITS_EXPORT plus_minus : opt< one< '+', '-' > > {};
struct UNITS_EXPORT dot : one< '.' > {};
struct UNITS_EXPORT inf : seq< TAO_PEGTL_ISTRING("inf"),
                  opt< TAO_PEGTL_ISTRING("inity") > > {};
struct UNITS_EXPORT nan : seq< TAO_PEGTL_ISTRING("nan"),
                  opt< one< '(' >,
                       plus< alnum >,
                       one< ')' > > > {};
template< typename D >
struct number :
  if_then_else< dot,
                plus< D >,
                seq< plus< D >, opt< dot, star< D > > >
                > {};
struct UNITS_EXPORT e : one< 'e', 'E' > {};
struct UNITS_EXPORT p : one< 'p', 'P' > {};
struct UNITS_EXPORT exponent : seq< plus_minus, plus< tao::pegtl::digit > > {};
struct UNITS_EXPORT decimal : seq< number< tao::pegtl::digit >, opt< e, exponent > > {};
struct UNITS_EXPORT hexadecimal : seq< one< '0' >, one< 'x', 'X' >, number< xdigit >,
                          opt< p, exponent > > {};
struct UNITS_EXPORT value : seq< plus_minus, sor< hexadecimal, decimal, inf, nan > > {};
// -----

// A keyword indicating translation from some baseline.
struct UNITS_EXPORT shift_kw : seq<
  star<white_space>,
  sor<
    one<'@'>,
    TAO_PEGTL_ISTRING("after"),
    TAO_PEGTL_ISTRING("from"),
    TAO_PEGTL_ISTRING("ref"),
    TAO_PEGTL_ISTRING("since")
  >,
  star<white_space>> {};

struct UNITS_EXPORT divide_kw : seq<
  star<white_space>,
  sor<
    one<'/'>,
    TAO_PEGTL_ISTRING("per")
  >,
  star<white_space>> {};

struct UNITS_EXPORT multiply_kw :
  seq<
    sor<
      seq<
        star<white_space>,
        sor<
          TAO_PEGTL_ISTRING("*"),
          TAO_PEGTL_ISTRING("×"),
          TAO_PEGTL_ISTRING("·")
        >
      >,
      one<' '>
    >,
    star<white_space>
  > {};

// Accept either a^b or a**b.
struct UNITS_EXPORT exponent_kw : sor<one<'^'>, TAO_PEGTL_ISTRING("**")> {};

// Parentheses can group expressions
struct UNITS_EXPORT lparen_kw : one<'('> {};
struct UNITS_EXPORT rparen_kw : one<')'> {};

struct UNITS_EXPORT basic_first :
  sor<
    ranges<'a', 'z', 'A', 'Z'>,
    TAO_PEGTL_ISTRING("‘"),
    TAO_PEGTL_ISTRING("’"),
    TAO_PEGTL_ISTRING("“"),
    TAO_PEGTL_ISTRING("”"),
    TAO_PEGTL_ISTRING("°"),
    TAO_PEGTL_ISTRING("Θ"),
    TAO_PEGTL_ISTRING("θ"),
    TAO_PEGTL_ISTRING("φ"),
    TAO_PEGTL_ISTRING("Ω"),
    TAO_PEGTL_ISTRING("㏀"),
    TAO_PEGTL_ISTRING("㏁"),
    TAO_PEGTL_ISTRING("℧"),
    TAO_PEGTL_ISTRING("℉"),
    TAO_PEGTL_ISTRING("℃"),
    TAO_PEGTL_ISTRING("K"),
    TAO_PEGTL_ISTRING("Å"),
    TAO_PEGTL_ISTRING("µ")
  > {};

struct UNITS_EXPORT basic_expression :
  seq<
    basic_first,
    star<
      sor<
        basic_first,
        one<'_'>,
        one<'-'>
      >
    >
  > {};

struct UNITS_EXPORT power_expression :
  sor<
    seq<basic_expression, exponent_kw, exponent>,
    // seq<basic_expression, exponent>,
    basic_expression
  > {};

struct UNITS_EXPORT product_expression;

struct UNITS_EXPORT paren_expression : seq<lparen_kw, seq<product_expression>, rparen_kw> {};

struct UNITS_EXPORT product_expression :
  sor<
    paren_expression,
    seq<power_expression, multiply_kw, product_expression>,
    seq<power_expression,   divide_kw, product_expression>,
    power_expression
  > {};

struct UNITS_EXPORT shifted_expression :
  sor<
    seq<product_expression, shift_kw, value>,
    //, seq<product_expression, shift_kw, timestamp>
    product_expression
  > {};

struct UNITS_EXPORT units : must<shifted_expression, eof> {};
struct UNITS_EXPORT measurement
  : must<
      seq<
        value,
        star<white_space>,
        opt<
          shifted_expression
        >
      >,
      eof
    > {};

} // namespace grammar
units_CLOSE_NAMESPACE

#endif // units_grammar_Grammar_h
