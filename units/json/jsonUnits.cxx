#include "units/json/jsonUnits.h"

#include "units/json/Helper.h"

#include "units/string/Manager.h"
#include "units/string/Token.h"

#include "units/Conversion.h"
#include "units/Dimension.h"
#include "units/Entity.h"
#include "units/PreferredUnits.h"
#include "units/Prefix.h"
#include "units/System.h"
#include "units/Unit.h"
#include "units/Visit.h"

#include "units/grammar/Functions.h"

#include <iostream>
#include <set>
#include <thread>

using namespace units::string::literals;

units_BEGIN_NAMESPACE

namespace string
{

void from_json(const nlohmann::json& jj, std::shared_ptr<Manager>& mm)
{
  if (!mm.get() || jj.is_null())
  {
    mm = units::string::Manager::create();
  }
  // Warn if we have 64-bit hash codes on a 32-bit machine:
  auto sit = jj.find("hash_size");
  if (sit != jj.end())
  {
    auto jsonSize = sit->get<std::size_t>();
    if (jsonSize > sizeof(std::size_t))
    {
      std::cerr <<
        "Deserializing " << (jsonSize * 8)
                         << "-bit hash codes "
                            "on a "
                         << (sizeof(std::size_t) * 8)
                         << "-bit platform "
                            "will likely not work. Expect downstream failures.";
    }
  }
  // NB: We do not call mm->reset() here since people may
  // wish to deserialize multiple files to the same manager.
  auto mit = jj.find("members");
  if (mit != jj.end())
  {
    for (const auto& element : mit->items())
    {
      Hash oldHash = element.value().get<Hash>();
      Hash newHash = mm->manage(element.key());
      if (newHash != oldHash)
      {
        std::lock_guard<std::mutex> writeLock(mm->m_writeLock);
        mm->m_translation[oldHash] = newHash;
      }
    }
    auto sit = jj.find("sets");
    if (sit != jj.end())
    {
      for (const auto& element : sit->items())
      {
        Hash oldSetHash = static_cast<Hash>(stoull(element.key(), nullptr));
        auto it = mm->m_translation.find(oldSetHash);
        Hash newSetHash = it == mm->m_translation.end() ? oldSetHash : it->second;
        auto oldMembers = element.value().get<std::unordered_set<Hash>>();
        for (const auto& oldMember : oldMembers)
        {
          it = mm->m_translation.find(oldMember);
          mm->insert(newSetHash, it == mm->m_translation.end() ? oldMember : it->second);
        }
      }
    }
  }
}

void to_json(nlohmann::json& jj, const std::shared_ptr<Manager>& mm)
{
  nlohmann::json::object_t members;
  nlohmann::json::object_t sets;

  // Record the size (in bytes) of hashes on this platform.
  // This will be 4 or 8 (32 or 64 bits).
  jj["hash_size"] = sizeof(std::size_t);

  mm->visitMembers([&members, &mm](Hash h) {
    // We store the "inverse" of the string-manager's map because
    // JSON keys must be strings and we would rather avoid the overhead
    // and potential ambiguities of conversion where possible, although
    // the sets below do some conversion.
    members[mm->value(h)] = h;
    return units::Visit::Continue;
  });
  jj["members"] = members;

  mm->visitSets([&sets, &mm](Hash setHash) {
    nlohmann::json::array_t children;
    mm->visitMembers(
      [&children](Hash h) {
        children.push_back(h);
        return units::Visit::Continue;
      },
      setHash);
    sets[std::to_string(setHash)] = children;
    return units::Visit::Continue;
  });
  if (!sets.empty())
  {
    jj["sets"] = sets;
  }
}

void from_json(const nlohmann::json& jj, Token& tt)
{
  if (jj.is_string())
  {
    tt = Token(jj.get<std::string>());
  }
  else
  {
    tt = Token::fromHash(jj.get<Hash>());
  }
}

void to_json(nlohmann::json& jj, const Token& tt)
{
  if (tt.hasData())
  {
    jj = tt.data();
  }
  else
  {
    jj = tt.id();
  }
}

} // namespace string

namespace
{

// Append \a dim and all of its basis dimensions to \a dimArr while removing them from \a allDims.
// This guarantees an ordering such that no secondary dimension is serialized before any of its
// primary dimensions.
void appendDim(
  nlohmann::json& dimArr,
  const std::shared_ptr<InternalDimension>& dim,
  std::set<std::shared_ptr<InternalDimension>>& allDims)
{
  // Prepend any dimensions in the basis (recursing as needed).
  if (auto compDim = std::dynamic_pointer_cast<CompositeDimension>(dim))
  {
    for (const auto& entry : compDim->m_basis)
    {
      auto basisDim = std::static_pointer_cast<InternalDimension>(entry.first->shared_from_this());
      if (allDims.find(basisDim) != allDims.end())
      {
        appendDim(dimArr, basisDim, allDims);
      }
    }
  }
  // Finally append the requested dimension and remove it from our working set.
  dimArr.push_back(dim);
  allDims.erase(dim);
}

} // anonymous namespace

void from_json(const nlohmann::json& jj, std::shared_ptr<InternalDimension>& dd)
{
  if (jj.contains("basis"))
  {
    dd = std::shared_ptr<InternalDimension>(new CompositeDimension);
    auto base = std::static_pointer_cast<Entity>(dd);
    from_json(jj, base);
    // parse basis
    auto& helper = json::Helper::instance();
    grammar::measurement_state state;
    state.parsing_dimensions = true;
    state.system = helper.system();
    try
    {
      if (!grammar::parseUnits(jj.at("basis"), state))
      {
        std::cerr << "Unable to parse basis for \"" << dd->m_name.data() << "\"\n";
      }
      std::static_pointer_cast<CompositeDimension>(dd)->m_basis = state.dimensions.m_data;
    }
    catch (tao::pegtl::parse_error& e)
    {
      std::cerr << "Unable to parse basis for \"" << dd->m_name.data() << "\": " << e.what() << "\n";
    }
    return;
  }
  dd = std::shared_ptr<InternalDimension>(new InternalDimension);
  auto base = std::static_pointer_cast<Entity>(dd);
  from_json(jj, base);
}

void to_json(nlohmann::json& jj, const std::shared_ptr<InternalDimension>& dd)
{
  to_json(jj, std::static_pointer_cast<Entity>(dd));
  if (const auto* derived = dynamic_cast<CompositeDimension*>(dd.get()))
  {
    // Generate basis string
    std::ostringstream basis;
    basis << Dimension(derived->m_basis);
    jj["basis"] = basis.str();
  }
}

void from_json(const nlohmann::json& jj, std::shared_ptr<Entity>& ee)
{
  if (!ee)
  {
    ee = std::shared_ptr<Entity>(new Entity);
  }
  ee->m_name = jj.at("name").get<std::string>();
  if (jj.contains("symbol"))
  {
    ee->m_symbol = jj.at("symbol").get<std::string>();
  }
  if (jj.contains("no-plural"))
  {
    ee->m_noPlural = jj.at("no-plural").get<bool>();
  }
  if (jj.contains("plural"))
  {
    if (ee->m_noPlural)
    {
      std::cerr
        << "ERROR: Entity \"" << ee->m_name.data() << "\""
        << " is marked as not having a plural but one was specified.\n";
    }
    ee->m_plural = jj.at("plural").get<std::string>();
  }
  if (jj.contains("description"))
  {
    ee->m_description = jj.at("description").get<std::string>();
  }
  if (jj.contains("symbol-aliases"))
  {
    ee->m_symbolAliases = jj.at("symbol-aliases").get<std::unordered_set<units::string::Token>>();
  }
  if (jj.contains("name-aliases"))
  {
    ee->m_nameAliases = jj.at("name-aliases").get<std::unordered_set<units::string::Token>>();
  }
}

void to_json(nlohmann::json& jj, const std::shared_ptr<Entity>& ee)
{
  if (!ee) { return; }

  jj["name"] = ee->m_name;
  if (ee->m_symbol.valid())
  {
    jj["symbol"] = ee->m_symbol;
  }
  if (ee->m_noPlural)
  {
    jj["no-plural"] = true;
  }
  else
  {
    if (ee->m_plural.valid())
    {
      jj["plural"] = ee->m_plural;
    }
  }
  if (ee->m_description.valid())
  {
    jj["description"] = ee->m_description;
  }
  if (!ee->m_symbolAliases.empty())
  {
    jj["symbol-aliases"] = ee->m_symbolAliases;
  }
  if (!ee->m_nameAliases.empty())
  {
    jj["name-aliases"] = ee->m_nameAliases;
  }
}

void from_json(const nlohmann::json& jj, std::shared_ptr<InternalPrefix>& pp)
{
  if (!pp)
  {
    pp = std::shared_ptr<InternalPrefix>(new InternalPrefix);
  }
  auto base = std::static_pointer_cast<Entity>(pp);
  from_json(jj, base);
  if (jj.contains("base") && jj.contains("exponent"))
  {
    pp->m_base = jj.at("base").get<double>();
    pp->m_exponent = jj.at("exponent").get<double>();
    pp->m_factor = std::pow(pp->m_base, pp->m_exponent);
  }
  else if (jj.contains("factor"))
  {
    pp->m_factor = jj.at("factor").get<double>();
    pp->m_base = 10.;
    pp->m_exponent = std::log10(pp->m_factor);
  }
  else
  {
    throw std::logic_error("A prefix must specify its base and exponent.");
  }
}

void to_json(nlohmann::json& jj, const std::shared_ptr<InternalPrefix>& pp)
{
  auto base = std::static_pointer_cast<Entity>(pp);
  to_json(jj, base);
  jj["base"] = pp->m_base;
  jj["exponent"] = pp->m_exponent;
}

void from_json(const nlohmann::json& jj, std::shared_ptr<System>& ss)
{
  if (!ss)
  {
    ss = std::make_shared<System>();
  }

  json::Helper::pushInstance(ss);
  auto prefixIt = jj.find("prefixes");
  if (prefixIt != jj.end())
  {
    auto prefixes = prefixIt->get<std::vector<std::shared_ptr<InternalPrefix>>>();
    for (const auto& prefix : prefixes)
    {
      // std::cout << "Add prefix " << prefix->m_symbol.data() << " = " << prefix->m_base << "^" << prefix->m_exponent << "\n";
      ss->addPrefix(prefix);
    }
  }

  auto dimIt = jj.find("dimensions");
  if (dimIt != jj.end())
  {
    for (const auto& dimEntry : *dimIt)
    {
      auto internalDim = dimEntry.get<std::shared_ptr<InternalDimension>>();
      ss->addDimension(internalDim);
      // std::cout << "Add dimension " << internalDim->m_symbol.data() << " " << Dimension({*internalDim, 1}) << "\n";
    }
  }

  auto unitIt = jj.find("units");
  if (unitIt != jj.end())
  {
    auto internalUnits = unitIt->get<std::vector<std::shared_ptr<InternalUnit>>>();
    for (const auto& unit : internalUnits)
    {
      ss->addUnit(unit);
      // std::cout << "Add unit " << unit->m_name.data() << " " << Unit({*unit, 1}) << "\n";
    }
  }

  auto cvtIt = jj.find("conversions");
  if (cvtIt != jj.end())
  {
    auto conversions = cvtIt->get<std::vector<std::shared_ptr<Conversion>>>();
    for (const auto& conversion : conversions)
    {
      ss->addConversion(conversion);
    }
  }

  auto ctxIt = jj.find("unit-contexts");
  if (ctxIt != jj.end())
  {
    for (const auto& item : ctxIt->items())
    {
      ss->m_unitContexts[item.key()] = item.value().get<std::shared_ptr<PreferredUnits>>();
    }

    auto actIt = jj.find("active-context");
    if (actIt != jj.end())
    {
      ss->m_activeUnitContext = actIt->get<std::string>();
    }
  }

  json::Helper::popInstance();
}

void to_json(nlohmann::json& jj, const std::shared_ptr<System>& ss)
{
  auto prefixArr = nlohmann::json::array();
  for (const auto& entry : ss->m_prefixes)
  {
    prefixArr.push_back(entry.second);
  }
  jj["prefixes"] = prefixArr;

  // Order dimensions so that no composite dimension appears before any of its basis dimensions.
  std::set<std::shared_ptr<InternalDimension>> allDims;
  for (const auto& entry : ss->m_dimensions)
  {
    allDims.insert(entry.second);
  }
  auto dimArr = nlohmann::json::array();
  while (!allDims.empty())
  {
    appendDim(dimArr, *allDims.begin(), allDims);
  }
  jj["dimensions"] = dimArr;

  auto unitsArr = nlohmann::json::array();
  for (const auto& entry : ss->m_units)
  {
    unitsArr.push_back(entry);
  }
  jj["units"] = unitsArr;

  if (!ss->m_unitContexts.empty())
  {
    auto juc = nlohmann::json::object();
    for (const auto& entry : ss->m_unitContexts)
    {
      juc[entry.first.data()] = entry.second;
    }
    jj["unit-contexts"] = juc;
  }

  if (ss->m_activeUnitContext.valid())
  {
    jj["active-context"] = ss->m_activeUnitContext.data();
  }
}

void from_json(const nlohmann::json& jj, std::shared_ptr<InternalUnit>& uu)
{
  auto& helper = json::Helper::instance();

  auto jDimIt = jj.find("dimension");
  if (jDimIt == jj.end())
  {
    throw std::logic_error("No dimension provided for unit.");
  }
  auto sys = helper.system();
  auto dimIt = sys->m_dimensions.find(*jDimIt);
  if (dimIt == sys->m_dimensions.end())
  {
    std::ostringstream errMsg;
    errMsg
      << "Unknown dimension \"" << jDimIt->get<units::string::Token>().data() << "\""
      << " provided for unit " << jj.at("name") << ".";
    throw std::logic_error(errMsg.str());
  }
  Dimension unitDims({*dimIt->second, 1});

  // OK, it is safe to create a unit.
  uu = std::shared_ptr<InternalUnit>(new InternalUnit);
  auto base = std::static_pointer_cast<Entity>(uu);
  from_json(jj, base);

  uu->m_dimensions = unitDims;
}

void to_json(nlohmann::json& jj, const std::shared_ptr<InternalUnit>& uu)
{
  auto base = std::static_pointer_cast<Entity>(uu);
  to_json(jj, base);
  std::ostringstream dim;
  dim << uu->m_dimensions;
  jj["dimension"] = dim.str();
}

void from_json(const nlohmann::json& jj, std::shared_ptr<Conversion>& cc)
{
  auto& helper = json::Helper::instance();

  auto jSrcIt = jj.find("source");
  auto jTgtIt = jj.find("target");
  if (jSrcIt == jj.end() || jTgtIt == jj.end())
  {
    throw std::logic_error("No source or target unit provided for conversion.");
  }
  auto jRuleIt = jj.find("rule");
  if (jRuleIt == jj.end())
  {
    throw std::logic_error("No rule provided for conversion.");
  }

  auto sys = helper.system();
  bool ok = false;
  auto src = sys->unit(jSrcIt->get<std::string>(), &ok);
  if (!ok)
  {
    std::ostringstream msg;
    msg << "Invalid source unit string \"" << jSrcIt->get<std::string>() << "\".";
    throw std::logic_error(msg.str());
  }
  auto tgt = sys->unit(jTgtIt->get<std::string>(), &ok);
  if (!ok)
  {
    std::ostringstream msg;
    msg << "Invalid target unit string \"" << jTgtIt->get<std::string>() << "\".";
    throw std::logic_error(msg.str());
  }
  auto rule = jRuleIt->get<std::shared_ptr<Rule>>();
  if (!rule)
  {
    throw std::logic_error("Invalid rule.");
  }
  cc = std::make_shared<Conversion>(src, tgt, rule);
}

void to_json(nlohmann::json& jj, const std::shared_ptr<Conversion>& cc)
{
  std::ostringstream srcUnit;
  srcUnit << cc->m_source;
  jj["source"] = srcUnit.str();

  std::ostringstream tgtUnit;
  tgtUnit << cc->m_target;
  jj["target"] = tgtUnit.str();

  jj["rule"] = cc->m_rule;
}

void from_json(const nlohmann::json& jj, std::shared_ptr<Rule>& rr)
{
  auto jTypeIt = jj.find("type");
  if (jTypeIt == jj.end())
  {
    throw std::logic_error("Missing rule type.");
  }
  auto typeToken = jTypeIt->get<units::string::Token>();
  switch (typeToken.id())
  {
  case "factor"_hash:
    rr = std::make_shared<FactorRule>(jj["factor"].get<double>());
    break;
  case "offset"_hash:
    rr = std::make_shared<OffsetRule>(jj["factor"].get<double>(), jj["offset"].get<double>());
    break;
  case "log"_hash:
    rr = std::make_shared<LogRule>(jj["factor"].get<double>(), jj["base"].get<int>());
    break;
  default:
    {
      std::ostringstream msg;
      msg << "Unsupported rule type \"" << typeToken.data() << "\".";
      throw std::logic_error(msg.str());
    }
  }
}

void to_json(nlohmann::json& jj, const std::shared_ptr<Rule>& rr)
{
  if (auto factorRule = std::dynamic_pointer_cast<FactorRule>(rr))
  {
    jj["type"] = "factor";
    jj["factor"] = factorRule->m_factor;
  }
  else if (auto offsetRule = std::dynamic_pointer_cast<OffsetRule>(rr))
  {
    jj["type"] = "offset";
    jj["factor"] = offsetRule->m_factor;
    jj["offset"] = offsetRule->m_offset;
  }
  else if (auto logRule = std::dynamic_pointer_cast<LogRule>(rr))
  {
    jj["type"] = "log";
    jj["factor"] = logRule->m_factor;
    jj["base"] = logRule->m_base;
  }
  else
  {
    throw std::logic_error("Unsupported rule type or null rule.");
  }
}

void from_json(const nlohmann::json& jj, std::shared_ptr<PreferredUnits>& pp)
{
  auto& helper = json::Helper::instance();
  auto sys = helper.system();
  if (!pp)
  {
    pp = PreferredUnits::create(sys.get(), /* empty */true);
  }
  auto jit = jj.find("user-editable");
  if (jit != jj.end())
  {
    pp->m_userEditable = jit->get<bool>();
  }
  jit = jj.find("units");
  if (jit == jj.end()) { return; }
  for (const auto& item : jit->items())
  {
    bool ok;
    auto dim = sys->dimension(item.key(), &ok);
    if (!ok)
    {
      std::cerr << "ERROR: Unable to find dimension " << item.key() << ".\n";
      continue;
    }
    PreferredUnits::PrioritizedUnits suggested;
    suggested.reserve(item.value().size());
    for (const auto& unitExpr : item.value())
    {
      auto label = unitExpr.get<std::string>();
      auto unit = sys->unit(label, &ok);
      if (!ok)
      {
        std::cerr << "ERROR: Unable to parse unit " << unitExpr << ".\n";
        continue;
      }
      suggested.push_back(PreferredUnits::UnitAndLabel{unit, label});
    }
    pp->m_unitsByDimension[dim] = suggested;
  }
}

void to_json(nlohmann::json& jj, const std::shared_ptr<PreferredUnits>& pp)
{
  if (!pp)
  {
    return;
  }
  jj["user-editable"] = pp->m_userEditable;
  nlohmann::json units;
  for (const auto& entry : pp->m_unitsByDimension)
  {
    auto suggested = nlohmann::json::array();
    for (const auto& unitAndLabel : entry.second)
    {
      suggested.push_back(unitAndLabel.m_label);
    }
    units[entry.first.name().data()] = suggested;
  }
  if (!units.empty())
  {
    jj["units"] = units;
  }
}

units_CLOSE_NAMESPACE
