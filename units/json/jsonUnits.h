#ifndef units_json_jsonUnits_h
#define units_json_jsonUnits_h

#include "units/Exports.h"
#include "units/Options.h"

#include "nlohmann/json.hpp"

#include <memory> // for std::shared_ptr<T>

units_BEGIN_NAMESPACE

namespace string
{

class Token;
class Manager;

void UNITS_EXPORT from_json(const nlohmann::json& jj, std::shared_ptr<Manager>& mm);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const std::shared_ptr<Manager>& mm);

void UNITS_EXPORT from_json(const nlohmann::json& jj, Token& tt);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const Token& tt);

}

struct Conversion;
struct Entity;
struct InternalDimension;
struct InternalPrefix;
struct PreferredUnits;
struct Rule;
struct System;
struct InternalUnit;

void UNITS_EXPORT from_json(const nlohmann::json& jj, std::shared_ptr<InternalDimension>& dd);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const std::shared_ptr<InternalDimension>& dd);

void UNITS_EXPORT from_json(const nlohmann::json& jj, std::shared_ptr<Entity>& ee);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const std::shared_ptr<Entity>& ee);

void UNITS_EXPORT from_json(const nlohmann::json& jj, std::shared_ptr<InternalPrefix>& pp);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const std::shared_ptr<InternalPrefix>& pp);

void UNITS_EXPORT from_json(const nlohmann::json& jj, std::shared_ptr<System>& ss);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const std::shared_ptr<System>& ss);

void UNITS_EXPORT from_json(const nlohmann::json& jj, std::shared_ptr<InternalUnit>& uu);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const std::shared_ptr<InternalUnit>& uu);

void UNITS_EXPORT from_json(const nlohmann::json& jj, std::shared_ptr<Conversion>& cc);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const std::shared_ptr<Conversion>& cc);

void UNITS_EXPORT from_json(const nlohmann::json& jj, std::shared_ptr<Rule>& rr);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const std::shared_ptr<Rule>& rr);

void UNITS_EXPORT from_json(const nlohmann::json& jj, std::shared_ptr<PreferredUnits>& pp);
void UNITS_EXPORT   to_json(nlohmann::json& jj, const std::shared_ptr<PreferredUnits>& pp);

units_CLOSE_NAMESPACE

#endif // units_json_jsonUnits_h
