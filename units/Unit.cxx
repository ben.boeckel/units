#include "units/Unit.h"

#include "units/Prefix.h"

#include <iostream>
#include <sstream>

units_BEGIN_NAMESPACE

InternalUnit::InternalUnit(
  const Dimension& dimensions,
  units::string::Token symbol,
  units::string::Token name,
  units::string::Token description)
  : Entity(symbol, name, description)
  , m_dimensions(dimensions)
{
}

InternalUnit::InternalUnit(
  const Dimension& dimensions,
  units::string::Token symbol,
  units::string::Token name,
  units::string::Token plural,
  units::string::Token description)
  : Entity(symbol, name, plural, description)
  , m_dimensions(dimensions)
{
}

Dimension InternalUnit::dimensionsFromBasis(const InternalUnit::Exponents& basis)
{
  Dimension result;
  for (const auto& entry : basis)
  {
    result = result * entry.first->m_dimensions.pow(entry.second);
  }
  return result;
}

Unit Unit::pow(double exponent) const
{
  Unit result;
  if (exponent == 0.)
  {
    return result; // Raising to the 0-th power removes all dimensions.
  }

  result = *this;
  // Multiply each exponent.
  for (auto& entry : result.m_data)
  {
    entry.second *= exponent;
  }
  for (auto& entry : result.m_powers)
  {
    entry.second *= exponent;
  }
  return result;
}

System* Unit::system() const
{
  if (m_data.empty())
  {
    return nullptr;
  }

  return m_data.begin()->first->m_parent;
}

std::string Unit::name() const
{
  std::ostringstream result;
  bool first = true;
  for (const auto& entry : m_data)
  {
    if (entry.first)
    {
      if (first)
      {
        first = false;
      }
      else
      {
        result << " ";
      }

      result << entry.first->m_name.data();
      if (entry.second != 1.0)
      {
        result << "^" << entry.second;
      }
    }
  }
  return result.str();
}

Dimension Unit::dimension() const
{
  Dimension result;
  for (const auto& entry : m_data)
  {
    if (entry.first)
    {
      result = result * entry.first->m_dimensions.pow(entry.second);
    }
  }
  return result;
}

bool Unit::dimensionless() const
{
  return m_data.empty();
}

bool Unit::simple() const
{
  return (m_data.size() == 1 && m_data.begin()->second == 1.0);
}

Unit Unit::canonical() const
{
  Unit result = *this;
  result.m_powers.clear();
  return result;
}

bool Unit::overlaps(const Unit& other) const
{
  for (const auto& entry : other.m_data)
  {
    if (m_data.find(entry.first) != m_data.end())
    {
      return true;
    }
  }
  return false;
}

double Unit::factor(const Unit& other) const
{
  double pf = -1.;
  double nf = +1.;
  for (const auto& entry : other.m_data)
  {
    auto it = m_data.find(entry.first);
    if (it == m_data.end() || std::abs(it->second) < std::abs(entry.second))
    {
      return 0.0;
    }

    double np = it->second * entry.second;
    double af = std::abs(it->second) / std::abs(entry.second);
    if (np > 0.)
    {
      if (nf < 0.)
      {
        // Other factors are negative, but this one is positive.
        return 0.0;
      }
      if (pf > af || pf < 0.)
      {
        pf = af;
      }
    }
    else if (np < 0.)
    {
      if (pf > 0.)
      {
        // Other factors are positive, but this one is negative.
        return 0.0;
      }
      if (nf < -af || nf > 0.)
      {
        nf = -af;
      }
    }
    else
    {
      std::cerr << "Impossible to factor!\n";
      return 0.0;
    }
  }
  return pf > 0. ? pf : nf;
}

Unit& Unit::operator *= (const Unit& other)
{
  // Compute products of units
  for (const auto& entry: other.m_data)
  {
    m_data[const_cast<InternalUnit*>(entry.first)] += entry.second;
  }
  // Compute products of prefix "powers"
  for (const auto& entry: other.m_powers)
  {
    m_powers[entry.first] += entry.second;
  }
  // Prune keys with zero values:
  for (auto it = m_data.begin(); it != m_data.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      m_data.erase(prev);
    }
  }
  for (auto it = m_powers.begin(); it != m_powers.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      m_powers.erase(prev);
    }
  }
  return *this;
}

Unit& Unit::operator /= (const Unit& other)
{
  // Subtract the "denominator"
  for (const auto& entry: other.m_data)
  {
    m_data[const_cast<InternalUnit*>(entry.first)] -= entry.second;
  }
  for (const auto& entry: other.m_powers)
  {
    m_powers[entry.first] -= entry.second;
  }
  // Prune keys with zero values:
  for (auto it = m_data.begin(); it != m_data.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      m_data.erase(prev);
    }
  }
  for (auto it = m_powers.begin(); it != m_powers.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      m_powers.erase(prev);
    }
  }

  return *this;
}

void Unit::dump() const
{
  std::cout << *this << "\n";
}

bool Unit::compareByNameAscending(const Unit& aa, const Unit& bb)
{
  auto na = aa.name();
  auto nb = bb.name();
  return na < nb;
}

Unit operator * (const Prefix& pp, const Unit& uu)
{
  if (!pp.m_prefix)
  {
    return uu;
  }

  Unit result = uu;
  result.m_powers[pp.m_prefix->m_base] += pp.m_prefix->m_exponent;
  if (result.m_powers[pp.m_prefix->m_base] == 0.)
  {
    result.m_powers.erase(pp.m_prefix->m_base);
  }
  return result;
}

Unit operator * (const Unit& a, const Unit& b)
{
  Unit result;
  // Compute products of units
  for (const auto& entry : a.m_data)
  {
    result.m_data[const_cast<InternalUnit*>(entry.first)] += entry.second;
  }
  for (const auto& entry: b.m_data)
  {
    result.m_data[const_cast<InternalUnit*>(entry.first)] += entry.second;
  }
  // Compute products of prefix "powers"
  for (const auto& entry : a.m_powers)
  {
    result.m_powers[entry.first] += entry.second;
  }
  for (const auto& entry: b.m_powers)
  {
    result.m_powers[entry.first] += entry.second;
  }
  // Prune keys with zero values:
  for (auto it = result.m_data.begin(); it != result.m_data.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      result.m_data.erase(prev);
    }
  }
  for (auto it = result.m_powers.begin(); it != result.m_powers.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      result.m_powers.erase(prev);
    }
  }
  return result;
}

Unit operator / (const Unit& aa, const Unit& bb)
{
  Unit result;
  // Add to the "numerator"
  for (const auto& entry : aa.m_data)
  {
    result.m_data[const_cast<InternalUnit*>(entry.first)] += entry.second;
  }
  for (const auto& entry : aa.m_powers)
  {
    result.m_powers[entry.first] += entry.second;
  }
  // Subtract from the "denominator"
  for (const auto& entry: bb.m_data)
  {
    result.m_data[const_cast<InternalUnit*>(entry.first)] -= entry.second;
  }
  for (const auto& entry: bb.m_powers)
  {
    result.m_powers[entry.first] -= entry.second;
  }
  // Prune keys with zero values:
  for (auto it = result.m_data.begin(); it != result.m_data.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      result.m_data.erase(prev);
    }
  }
  for (auto it = result.m_powers.begin(); it != result.m_powers.end();)
  {
    auto prev = it;
    ++it;
    if (prev->second == 0)
    {
      result.m_powers.erase(prev);
    }
  }

  return result;
}

bool operator == (const Unit& aa, const Unit& bb)
{
  return aa.m_data == bb.m_data && aa.m_powers == bb.m_powers;
}

bool operator != (const Unit& aa, const Unit& bb)
{
  return aa.m_data != bb.m_data || aa.m_powers != bb.m_powers;
}

bool operator < (const Unit& aa, const Unit& bb)
{
  return aa.m_data < bb.m_data ||
    (aa.m_data == bb.m_data && aa.m_powers < bb.m_powers);
}

units_CLOSE_NAMESPACE
