#ifndef units_Prefix_h
#define units_Prefix_h

#include "units/Entity.h"

units_BEGIN_NAMESPACE

/// Adjust the scale of a unit by some factor.
struct UNITS_EXPORT InternalPrefix : Entity
{
  InternalPrefix() = default;
  InternalPrefix(
    double base,
    double exponent,
    units::string::Token name,
    units::string::Token symbol,
    units::string::Token description = units::string::Token::invalid())
    : Entity(name, symbol, description)
    , m_factor(std::pow(base, exponent))
    , m_base(base)
    , m_exponent(exponent)
  {
  }

  double factor() const { return m_factor; }
  double base() const { return m_base; }
  double exponent() const { return m_exponent; }
  double m_factor;
  double m_base{ 10 };
  double m_exponent;
};

/// A user-facing scale factor that references a unit system's InternalPrefix.
struct UNITS_EXPORT Prefix
{
  Prefix() = default;
  Prefix(const Prefix& pp) : m_prefix(pp.m_prefix) { }
  Prefix(const InternalPrefix& pp) : m_prefix(&pp) { }
  Prefix(const std::shared_ptr<InternalPrefix>& pp) : m_prefix(pp.get()) { }
  ~Prefix() { m_prefix = nullptr; }

  const InternalPrefix* m_prefix{ nullptr };
};

units_CLOSE_NAMESPACE

#endif // units_Prefix_h
