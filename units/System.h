#ifndef units_System
#define units_System
/*!\file System.h */

#include "units/CompatibleUnitOptions.h"
#include "units/Conversion.h"
#include "units/Dimension.h"
#include "units/Measurement.h"
#include "units/Prefix.h"
#include "units/Unit.h"

#include <memory> // for std::shared_ptr<T>
#include <set> // for API
#include <unordered_map> // for API
#include <vector> // for API

/// The primary namespace of the units library.
units_BEGIN_NAMESPACE

struct Converter;
struct PreferredUnits;

/// A set of dimensions, units, prefixes, and conversions available for use by applications.
///
/// You may add dimensions, units, prefixes, and conversions as you like, but you
/// may not remove them; instead destroy the system and create a replacement.
/// There is not a good use case for removing these objects and it simplifies the design:
/// user-facing objects (which we do not track) refer to the underlying objects by raw pointer.
struct UNITS_EXPORT System : std::enable_shared_from_this<System>
{
  System();
  virtual ~System();

  /// Create an empty unit system that you can populate with addDimension() and addUnit().
  static std::shared_ptr<System> create();
  /// Create a unit system from a string holding a JSON specification.
  static std::shared_ptr<System> createFromSpec(const std::string& jsonSpec);
  /// Create a unit system initialized with the library-provided JSON specification.
  static std::shared_ptr<System> createWithDefaults();

  /// Create a new prefix managed by this unit system.
  Prefix createPrefix(
    int base,
    double exponent,
    string::Token symbol,
    string::Token name,
    string::Token plural = string::Token::invalid(),
    string::Token description = string::Token::invalid());

  /// Create a new dimension managed by this unit system.
  Dimension createDimension(
    string::Token symbol,
    string::Token name,
    string::Token plural = string::Token::invalid(),
    string::Token description = string::Token::invalid());

  /// Create a new unit managed by this unit system.
  Unit createUnit(
    const Dimension& dimension,
    string::Token symbol,
    string::Token name,
    string::Token plural = string::Token::invalid(),
    string::Token description = string::Token::invalid());

  /// Add a prefix to this system.
  ///
  /// This is called internally by createPrefix and the JSON deserializer.
  bool addPrefix(const std::shared_ptr<InternalPrefix>& pfx);

  /// Add an internal dimension to this system.
  ///
  /// This is called internally by createDimension and the JSON deserializer.
  bool addDimension(const std::shared_ptr<InternalDimension>& dim);

  /// Add an internal unit to this system.
  ///
  /// This is called internally by createDimension and the JSON deserializer.
  bool addUnit(const std::shared_ptr<InternalUnit>& dim);

  /// Add a unit conversion to this system.
  ///
  /// Conversions include a source and target Unit plus a rule that
  /// can be applied to a Measurement in either a forward or reverse
  /// sense (i.e., the conversion must be invertible).
  ///
  /// This is called internally by the JSON deserializer.
  bool addConversion(const std::shared_ptr<Conversion>& conversion);

  /// Find a dimension given its name or symbol.
  Dimension dimension(const std::string& dimensionString, bool* success = nullptr) const;

  /// Find a unit given its name or symbol.
  Unit unit(const std::string& unitString, bool* success = nullptr) const;

  /// Parse the \a text into a measurement.
  ///
  /// If this succeeds, the measurement is returned. Otherwise a
  /// measurement with a NaN value and no units is returned.
  /// If \a success is a non-null pointer, then the boolean is set
  /// to true if the measurement \a text was parsed successfully and
  /// false otherwise.
  Measurement measurement(const std::string& text, bool* success = nullptr);

  /// Return a converter that can be applied to convert values between a pair of units.
  ///
  /// If conversion is not possible because the units are incompatible and
  /// a boolean \a success pointer is passed, it will be set to false;
  /// otherwise it will be set to true.
  ///
  /// Since the returned converter can be applied to many values rather
  /// than just a single value, this is preferred to creating many measurements
  /// with identical units and calling System::convert() on each.
  std::unique_ptr<Converter> convert(const Unit& source, const Unit& target) const;

  /// Convert a \a measurement into different \a units.
  ///
  /// If conversion is not possible because the units are incompatible and
  /// a boolean \a success pointer is passed, it will be set to false;
  /// otherwise it will be set to true.
  Measurement convert(const Measurement& source, const Unit& target, bool* success = nullptr) const;

  /// For a given \a unit, return a vector of all the compatible units (including \a unit itself).
  std::vector<Unit> compatibleUnits(
    const Unit& unit,
    const CompatibleUnitOptions& options = CompatibleUnitOptions()) const;

  /// A map of all prefixes (from both name and symbol tokens to a Prefix pointer).
  std::unordered_map<units::string::Token, std::shared_ptr<InternalPrefix>> m_prefixes;

  /// A map of all dimensions (from both name and symbol tokens to an InternalDimension pointer).
  ///
  /// Note that this map includes entries for both basic (InternalDimension) and composite
  /// (CompositeDimension) objects.
  std::unordered_map<units::string::Token, std::shared_ptr<InternalDimension>> m_dimensions;

  /// A map from Dimension to CompositeDimension pointers.
  ///
  /// This map can be used to discover the name and symbol for any Dimension that is not
  /// "simple". Note that this is a multimap as some composite dimensions have multiple
  /// names.
  /// For example, dimensionless numbers may be (a) truly void of dimension or (b) angles
  /// measured in radians or degrees or (c) strains (measured as length per unit length).
  /// It is sometimes convenient to group dimensionless units together under a label specific
  /// to their interpretation.
  std::map<Dimension, std::shared_ptr<CompositeDimension>> m_compositeDimensions;

  /// A vector of all registered units.
  ///
  /// This vector is responsible for ownership (via shared pointer) and ordering
  /// of units for use in linear solves done by conversion.
  std::vector<std::shared_ptr<InternalUnit>> m_units;

  /// A map of all units (from both name and symbol tokens to an InternalUnit pointer).
  std::unordered_map<units::string::Token, InternalUnit*> m_unitsByName;

  /// A map of all units organized by their dimension.
  ///
  /// This is used to identify sets of conformal units available for autocompletion.
  std::map<Dimension, std::unordered_set<InternalUnit*>> m_indexByDimension;

  /// Named sets of user- and/or workflow-preferred units.
  ///
  /// This object holds sets of units in order of their user- and/or application-assigned priority.
  /// Each set is a "context" and has a user-presentable name (e.g., "SI (mks)").
  /// This member variable exists primarily for user interfaces rather than providing any
  /// particular functionality for dimensional analysis.
  ///
  /// Note that unlike m_indexByDimension, the unit contexts may include
  /// composite units (e.g., "m/s", "W m/K^2", etc.) in a dimension's
  /// prioritized vector of units.
  std::unordered_map<units::string::Token, std::shared_ptr<PreferredUnits>> m_unitContexts;

  /// The "active" set of preferred units.
  ///
  /// Calls to compatibleUnits() will return the list from this entry in m_unitContexts
  /// unless told not to.
  units::string::Token m_activeUnitContext;

  /// A vector of all registered unit conversions.
  ///
  /// This is used to maintain ownership of the conversions and their rules.
  /// It is also used to order and index conversions for use in linear solves.
  std::vector<std::shared_ptr<Conversion>> m_conversions;

  /// A map of all conversions indexed by each participating source and target unit.
  ///
  /// The keys of the map are all units with an empty m_powers (even if the power
  /// of the corresponding source/target unit is non-empty); this allows a uniform
  /// way to query independent of power.
  ///
  /// If a Conversion \a cc had a source of "kJ" and a target "kN m", then 3 entries would
  /// be added to m_conversionsByUnit:
  /// + "J" → (cc, true)
  /// + "N" → (cc, false)
  /// + "m" → (cc, false)
  std::map<InternalUnit*, std::set<std::pair<Conversion*, bool>>> m_conversionsByUnit;

  struct Internal;
  /// Private data (with Eigen depenedencies)
  Internal* m_p{ nullptr };
};

units_CLOSE_NAMESPACE

#endif // units_System
