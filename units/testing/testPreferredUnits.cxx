#include "units/PreferredUnits.h"
#include "units/System.h"

#include "units/json/jsonUnits.h"

#include <iostream>

using namespace units;

int testPreferredUnits(int argc, char* argv[])
{
  (void)argc;
  (void)argv;
  bool ok = true;

  auto sys = System::createWithDefaults();

  if (sys->m_unitContexts.size() != 1)
  {
    std::cerr
      << "ERROR: default system should have 1 unit context, "
      << "found " << sys->m_unitContexts.size() << ".\n";
    ok = false;
  }

  auto ctxt1 = PreferredUnits::create(sys.get(), true);
  if (!ctxt1 || !ctxt1->m_unitsByDimension.empty())
  {
    std::cerr << "ERROR: Expected to create an empty unit context.\n";
    ok = false;
  }
  auto ctxt2 = PreferredUnits::create(sys.get(), false);
  if (!ctxt2 || ctxt2->m_unitsByDimension.empty())
  {
    std::cerr << "ERROR: Expected to create a preconfigured unit context.\n";
    ok = false;
  }
  // Convert the pre-configured unit context into JSON and dump it for
  // inspection and pasting into hand-edited unit system specifications.
  nlohmann::json jj = ctxt2;
  std::cout << jj.dump(2) << "\n";

  return ok ? 0 : 1;
}
