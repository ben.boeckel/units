set(tests
  testBasics
  testJSON
  testConversion
  testPreferredUnits
)

if (units_ENABLE_TESTING)

  foreach(test ${tests})
    list(APPEND testImpls ${test}.cxx)
  endforeach()

  create_test_sourcelist(sources unitsCxxTests.cxx ${testImpls})
  add_executable(unitsCxxTests ${sources})
  target_link_libraries(unitsCxxTests PRIVATE units)
  target_include_directories(unitsCxxTests
    PRIVATE
      ${CMAKE_CURRENT_BINARY_DIR}
  )

  foreach (test ${tests})
    add_test(NAME ${test} COMMAND unitsCxxTests ${test})
    set_tests_properties(${test} PROPERTIES TIMEOUT 120)
  endforeach()

endif(units_ENABLE_TESTING)
