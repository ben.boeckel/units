#include "units/System.h"
#include "units/string/Token.h"

#include <iostream>

bool testFactor(
  const units::System* sys,
  const std::string& u1s,
  const std::string& u2s,
  double expected)
{
  bool didParse = false;
  auto u1 = sys->unit(u1s, &didParse);
  if (!didParse)
  {
    std::cerr << "    ERROR: Could not parse " << u1s << ".\n";
    return false;
  }

  auto u2 = sys->unit(u2s, &didParse);
  if (!didParse)
  {
    std::cerr << "    ERROR: Could not parse " << u2s << ".\n";
    return false;
  }

  double u12 = u2.factor(u1);
  std::cout << "  Factor of " << u1 << " in " << u2 << " = " << u12 << "\n";
  if (std::abs(u12 - expected) > 1e-10)
  {
    std::cerr << "    ERROR: Expected " << expected << "\n";
    return false;
  }
  return true;
}

bool testUnitFactorization(units::System* sys)
{
  std::cout << "Unit factor computations:\n";
  bool ok = true;
  // Test simple factorization:
  ok &= testFactor(sys, "g m^2", "g m^3 / s^2", 1);
  ok &= testFactor(sys, "g m^2", "g^-3 m^-4", -2);
  ok &= testFactor(sys, "m^-2", "g / m^2", 1);

  // Test simple factorization with mixed signs and absent factors:
  ok &= testFactor(sys, "g m^2", "g / m^2", 0);
  ok &= testFactor(sys, "g m^2", "m^-2", 0);

  // Unit::factor() should not attempt to apply conversions:
  ok &= testFactor(sys, "m", "N", 0); // not 1.
  ok &= testFactor(sys, "m", "N m", 1); // not 2.

  // Test fractional factor:
  ok &= testFactor(sys, "m^2 ft", "m^-3 ft^-4", -1.5);
  ok &= testFactor(sys, "m^2 ft", "m^3 ft^-4", 0);

  return true;
}

bool testDefaultSys()
{
  auto sys = units::System::createWithDefaults();

  std::string input = "m";
  bool parsedOK = false;
  auto unit = sys->unit(input, &parsedOK);
  if (!parsedOK || unit.dimensionless())
  {
    std::cerr << "Didn't parse " << input << std::endl;
    return false;
  }

  // Sanity check
  if (unit.system() == nullptr)
  {
    std::cerr << "ERROR: unit.system() is null\n";
    return false;
  }

  return true;
}

bool testPluralForms()
{
  auto sys = units::System::createWithDefaults();
  bool ok = true;
  bool didParse;

  // Test that default-plural dimensions are registered
  auto dim = sys->dimension("length", &didParse);
  if (!didParse) { std::cerr << "ERROR: No singular \"length\" dimension.\n"; }
  ok &= didParse;

  dim = sys->dimension("lengths", &didParse);
  if (!didParse) { std::cerr << "ERROR: No plural \"lengths\" dimension.\n"; }
  ok &= didParse;

  // Test that user-provided-plural dimensions are registered
  dim = sys->dimension("energy", &didParse);
  if (!didParse) { std::cerr << "ERROR: No singular \"energy\" dimension.\n"; }
  ok &= didParse;

  dim = sys->dimension("energies", &didParse);
  if (!didParse) { std::cerr << "ERROR: No plural \"energies\" dimension.\n"; }
  ok &= didParse;

  // Test that default-plural units are registered
  auto unit = sys->unit("meter", &didParse);
  if (!didParse) { std::cerr << "ERROR: No singular \"meter\" unit.\n"; }
  ok &= didParse;

  unit = sys->unit("meters", &didParse);
  if (!didParse) { std::cerr << "ERROR: No plural \"meters\" unit.\n"; }
  ok &= didParse;

  // Test that user-provided-plural units are registered
  unit = sys->unit("inch", &didParse);
  if (!didParse) { std::cerr << "ERROR: No singular \"inch\" unit.\n"; }
  ok &= didParse;

  unit = sys->unit("inches", &didParse);
  if (!didParse) { std::cerr << "ERROR: No plural \"inches\" unit.\n"; }
  ok &= didParse;

  return ok;
}

bool testWhitespaceParsing()
{
  auto sys = units::System::createWithDefaults();
  bool ok = true;
  bool didParse;

  // Test that dimensions with whitespaces in their names are parsed.
  auto dim = sys->dimension("electric current", &didParse);
  if (!didParse) { std::cerr << "ERROR: Could not find dimension with whitespace in its name.\n"; }
  ok &= didParse;

  std::vector<std::string> tests{
    "W/m*K",
    "W / m * K",
    "W / m* K",
    "W / m *K",
    "W/ m* K"
  };
  units::Unit unit;
  for (const auto& test : tests)
  {
    unit = sys->unit(test, &didParse);
    if (!didParse) { std::cerr << "ERROR: Could not parse \"" << test << "\" (whitespace check).\n"; }
    ok &= didParse;
  }

  return ok;
}

#if 0
bool testParenthesisParsing()
{
  auto sys = units::System::createWithDefaults();
  bool ok = true;
  bool didParse;

  std::vector<std::string> tests{
    "(kW)/m^2*K",
    "(W/m^2*K)",
    "W / (m^2 * K)",
    "W / m^2* K",
    "W / m^2 *K",
    "W/ m^2* K"
  };
  units::Unit unit;
  for (const auto& test : tests)
  {
    unit = sys->unit(test, &didParse);
    if (!didParse) { std::cerr << "ERROR: Could not parse \"" << test << "\" (parenthesis check).\n"; }
    ok &= didParse;
  }

  return ok;
}
#endif // 0

int testBasics(int argc, char* argv[])
{
  using namespace units::string::literals; // for ""_token
  bool ok = true;

  ok &= testDefaultSys();

  // Construct an empty unit system.
  auto sys = std::make_shared<units::System>();

  // Add a prefix to it.
  auto milli = sys->createPrefix(10, -3, "milli", "m");
  auto kilo = sys->createPrefix(10, +3, "kilo", "k");

  // Add some (internal, basic) dimensions to it:
  auto mass = sys->createDimension("mass", "M", "Mass of a substance.");
  auto length = sys->createDimension("length", "L", "Distance in a metric field.");
  auto time = sys->createDimension("time", "T", "Distance in a temporal field.");

  // Do "math" with the dimensions:
  auto speed = length / time;
  std::cout << "Speed has basic dimensions " << speed << "\n";
  auto force = mass * length / time.pow(2);
  std::cout << "Force has basic dimensions " << force << "\n";
  auto force2 = mass * speed / time;
  std::cout << "Force via speed also has basic dimensions " << force2 << "\n";
  if (force != force2)
  {
    std::cerr << "ERROR: Expected force dimensions to match.\n";
    ok = false;
  }

  auto none = units::string::Token::invalid();
  auto gram = sys->createUnit(mass, "g", "gram", "Metric (cgs) measure of mass.");
  auto meter = sys->createUnit(length, "m", "meter", "Metric (mks) measure of length.");
  auto second = sys->createUnit(time, "s", "second", "Metric (mks and cgs) measure of time.");
  auto newton = sys->createUnit(force, "N", "Newton", "Metric (mks) measure of force.");
  auto foot = sys->createUnit(length, "ft", "foot", "feet", "Imperial measure of length derived from a monarch's shoe size.");
  auto inch = sys->createUnit(length, "in", "inch", "inches", "Imperial measure of length.");
  auto furlong = sys->createUnit(length, none, "furlong", "Horsie racing distance.");
  auto poundsforce = sys->createUnit(force, "lbf", "pounds-force", "Imperial measure of force.");
  auto millinewton = gram * meter / second;
  std::cout << "A millinNewton is " << millinewton << " with dimension " << millinewton.dimension() << "\n";
  auto fullnewton = (kilo * gram) * meter / second;
  std::cout << "A Newton is " << fullnewton << " with dimension " << fullnewton.dimension() << "\n";

  bool didParse = false;
  auto pu = sys->unit("mm", &didParse);
  std::cout << "Should be mm: " << pu << "\n";
  if (!didParse)
  {
    std::cerr << "ERROR: Could not parse 'mm'\n";
    ok = false;
  }

  auto measurement = sys->measurement("5 mm", &didParse);
  std::cout << "Should be 5 mm: " << measurement << "\n";
  if (!didParse)
  {
    std::cerr << "ERROR: Could not parse '5 mm'\n";
    ok = false;
  }

  // Let's visit all the units organized by their basic dimension.
  for (const auto& dimEntry : sys->m_indexByDimension)
  {
    std::cout << dimEntry.first << ":\n";
    for (const auto& entry : dimEntry.second)
    {
      std::cout << "  " << entry->m_name.data() << " (" << entry->m_symbol.data() << ")\n";
    }
  }

  ok &= testUnitFactorization(sys.get());

  ok &= testWhitespaceParsing();
  // ok &= testParenthesisParsing();

  return ok ? 0 : 1;
}
