#include "units/Conversion.h"
#include "units/Converter.h"
#include "units/System.h"

#include <iostream>

using namespace units;

bool testOneConversion(
  const std::shared_ptr<System>& sys,
  const std::string& measurementIn,
  const std::string& unitsOut,
  const std::string& expectedMeasurementOut)
{
  bool canConvert = false;
  auto mm = sys->measurement(measurementIn);
  auto tt = sys->unit(unitsOut);
  auto nn = sys->convert(mm, tt, &canConvert);
  auto oo = sys->measurement(expectedMeasurementOut);
  if (canConvert)
  {
    std::cout << "Converted " << mm << " to " << nn << ", expected " << oo << "\n";
  }
  else
  {
    std::cerr << "ERROR: Could not convert " << mm << " to " << tt << "\n";
  }

  bool unitsMatch = !canConvert || (nn.m_units == oo.m_units);
  if (!unitsMatch)
  {
    std::cerr << "ERROR: Converted units are unexpected.\n";
  }
  double diff = std::abs(nn.m_value - oo.m_value);
  bool valuesMatch = ((diff / std::abs(oo.m_value)) < 1e-5);
  if (!valuesMatch)
  {
    std::cerr
      << "ERROR: Converted value is off by " << diff
      << " (" << std::abs(diff/oo.m_value)*100.0 << "\%).\n";
  }

  return canConvert && unitsMatch && valuesMatch;
}

bool testBulkConversion(
  const std::shared_ptr<System>& sys,
  const std::string& sourceUnits,
  const std::string& targetUnits,
  const std::vector<double>& sourceData,
  const std::vector<double>& expected
)
{
  std::cout
    << "Test bulk conversion from " << sys->unit(sourceUnits)
    << " to " << sys->unit(targetUnits) << "\n";

  bool ok = false;
  if (sourceData.size() != expected.size())
  {
    std::cerr << "  ERROR: source and expected data size mismatch.\n";
    return ok;
  }
  auto converter = sys->convert(
    sys->unit(sourceUnits),
    sys->unit(targetUnits));
  if (!converter)
  {
    std::cerr << "  ERROR: Could not create a converter.\n";
    return ok;
  }
  ok = true;
  for (std::size_t ii = 0; ii < sourceData.size(); ++ii)
  {
    double value = converter->transform(sourceData[ii]);
    double err = std::abs((value - expected[ii]) / expected[ii]);
    if (err > 1e-5)
    {
      std::cerr
        << "  ERROR: value " << ii << " " << sourceData[ii] << " → " << value
        << " != " << expected[ii] << "\n";
      ok = false;
    }
  }

  return ok;
}

int testConversion(int argc, char* argv[])
{
  (void)argc;
  (void)argv;
  bool ok = true;

  auto sys = System::createWithDefaults();

  // Parse a non-existent unit to ensure failure occurs.
  auto invalid = sys->measurement("2 ftxx", &ok);
  if (ok)
  {
    std::cerr
      << "ERROR: invalid is " << invalid
      << " (dimensions " << invalid.m_units.dimension() << ")"
      << " ok " << (ok ? "T" :"F") << "\n";
  }
  else
  {
    ok = true;
  }

  // Create a fictitious unit with a particular conversion we will test.
  auto nk = sys->createUnit(sys->dimension("L").pow(0.5), "nektar", "nk");
  auto mkin = sys->addConversion(
    std::make_shared<Conversion>(nk*nk, sys->unit("in"),
      std::make_shared<FactorRule>(0.01))); // 100 nk^2 = 1 in.

  auto mm = sys->measurement("10.0 Newton");
  std::cout << "Starting with " << mm << "\n";

  bool canConvert = false;
  auto tt = sys->unit("lbf");
  auto nn = sys->convert(mm, tt, &canConvert);
  ok &= canConvert;
  if (canConvert)
  {
    std::cout << "Converted " << mm << " to " << nn << "\n";
  }
  else
  {
    std::cerr << "ERROR: Could not convert " << mm << " to " << tt << "\n";
  }

  ok &= testOneConversion(sys, "100 nk^2", "in", "1 in");
  // Test that units raised to powers are handled properly.
  ok &= testOneConversion(sys, "1 nk^4", "in^2", "0.0001 in^2");
  // Test that units with prefixes are scaled properly when raised to a power
  // (deci-nektars**2 should divide the expected 1 in by 100):
  ok &= testOneConversion(sys, "100 dnk^2", "in", ".01 in");

  // Test offset scale with absolute values:
  // First a simple conversion (degC and K apply a single Conversion)
  ok &= testOneConversion(sys, "10 K", "degC", "-263.15 degC");
  ok &= testOneConversion(sys, "100 degC", "K", "373.15 K");
  // Now a complex conversion (multiple Conversions and/or non-unit factor)
  ok &= testOneConversion(sys, "10 degR", "K", "5.55556 K");
  ok &= testOneConversion(sys, "10 degF", "K", "260.928 K");
  // Test offset scales with assumed relative values:
  ok &= testOneConversion(sys, "0.001 in/degF", "mm/K", "4.572e-02 mm/K");
  // Test indirect conversion:
  ok &= testOneConversion(sys, "3 fur/fortnight", "m/hr", "1.79614 m/hr");
  // Test indirect conversion with prefix specified on target.
  ok &= testOneConversion(sys, "3 fur/fortnight", "mm/s", "0.49893 mm/s");

  // Test that offsets are included on absolute measurements.
  ok &= testBulkConversion(sys, "degC", "K", {20.0, 25.0, 30.}, {293.15, 298.15, 303.15});
  // Test that offsets are **not** included on non-simple (thus, assumed-relative) measurements.
  ok &= testBulkConversion(sys, "degC^2", "K^2", {20.0, 25.0, 30.}, {20.0, 25.0, 30.0});
  // As above, but with non-trivial scale factor.
  ok &= testBulkConversion(sys, "degC^2", "degF^2", {20.0, 25.0, 30.}, {64.8, 81.0, 97.2});

  // Regression test: Asking for output with a prefix when the
  // conversion applies the same prefix would end up doubling
  // the prefix exponent.
  ok &= testOneConversion(sys, "1 qt", "mL", "946.35 mL");

  // Test relationships among derived dimensions
  ok &= testOneConversion(sys, "1 Joule/Coulomb", "V", "1 Volt");
  ok &= testOneConversion(sys, "1 volt", "joule/coulomb", "1 joule/coulomb"); // Test aliases for downcased names.
  ok &= testOneConversion(sys, "2 C/s", "A", "2 Ampere");
  ok &= testOneConversion(sys, "1 kg m^2 / A s^3", "V", "1 V");

  // TODO: These are difficult and won't work until we implement a better exhaustive
  //       search of the conversion rules provided by the least-squares linear solve.
  // ok &= testOneConversion(sys, "1 s/Ω", "Farad", "1 Farad");
  // ok &= testOneConversion(sys, "1 C^2/N m", "Farad", "1 Farad");

  return ok ? 0 : 1;
}
