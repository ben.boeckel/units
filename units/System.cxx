#include "units/System.h"
#include "units/SystemData.h"

#include "units/Converter.h"
#include "units/PreferredUnits.h"
#include "units/grammar/Functions.h"
#include "units/json/jsonUnits.h"

#include <Eigen/SparseCore>
#include <Eigen/SparseQR>

#include <thread>

// Uncomment the following to debug the unit conversion process.
// #define UNITS_DBG_CONVERT 1

units_BEGIN_NAMESPACE

/// Private data held by a unit system.
struct System::Internal
{
  /// Update the solver's matrix factorization if needed.
  /// This assumes the caller holds m_solverGuard.
  void updateFactorization()
  {
    // Check whether the matrix factorization reflects the current system's
    // units and conversions. Since we do not support removing units/conversions,
    // we can simply see whether the matrix size matches the system's size.
    if (
      m_system->m_conversions.size() == m_solver.cols() &&
      m_system->m_units.size() == m_solver.rows())
    {
      // We're up to date.
      return;
    }

    // Populate the conversion matrix
    // TODO: cache this.
    Eigen::SparseMatrix<double> CC(
      static_cast<Eigen::Index>(m_system->m_units.size()),
      static_cast<Eigen::Index>(m_system->m_conversions.size()));
    std::vector<Eigen::Triplet<double>> entries;
    entries.reserve(m_system->m_conversions.size() * 4); // This is an estimate.
    Eigen::Index cc = 0;
    for (const auto& conversion : m_system->m_conversions)
    {
      for (const auto& unitAndPower : conversion->m_target.m_data)
      {
        entries.push_back(Eigen::Triplet<double>(unitAndPower.first->m_index, cc, unitAndPower.second));
      }
      for (const auto& unitAndPower : conversion->m_source.m_data)
      {
        entries.push_back(Eigen::Triplet<double>(unitAndPower.first->m_index, cc, -unitAndPower.second));
      }
      ++cc;
    }
    CC.setFromTriplets(entries.begin(), entries.end());
    m_solver.compute(CC);
  }

  /// The unit system that owns this private data.
  System* m_system{ nullptr };

  /// A mutex that locks m_solver when updating or computing conversions.
  std::mutex m_solverGuard;
  /// A sparse QR factorization solver. Recommended for least squares solves.
  Eigen::SparseQR<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> m_solver;
};

System::System()
  : m_p(new Internal)
{
  m_p->m_system = this;
}

System::~System()
{
  m_p->m_system = nullptr;
  delete m_p;
}

std::shared_ptr<System> System::create()
{
  return std::make_shared<System>();
}

std::shared_ptr<System> System::createFromSpec(const std::string& jsonSpec)
{
  try
  {
    auto jdata = nlohmann::json::parse(jsonSpec);
    auto result = jdata;
    return result;
  }
  catch (std::exception& e)
  {
    std::cerr << "ERROR: " << e.what() << "\n";
  }
  return nullptr;
}

std::shared_ptr<System> System::createWithDefaults()
{
  auto data = systemData();
  return System::createFromSpec(data);
}

Prefix System::createPrefix(
  int base,
  double exponent,
  string::Token symbol,
  string::Token name,
  string::Token plural,
  string::Token description)
{
  if (!name.valid() && !symbol.valid())
  {
    throw std::invalid_argument("A name or symbol must be provided.");
  }
  if (!name.valid() && plural.valid())
  {
    throw std::invalid_argument("You must specify a singular name if you specify a plural form.");
  }
  if (name.valid() && m_prefixes.find(name) != m_prefixes.end())
  {
    throw std::invalid_argument("Duplicate prefix name.");
  }
  if (symbol.valid() && m_prefixes.find(symbol) != m_prefixes.end())
  {
    throw std::invalid_argument("Duplicate prefix symbol.");
  }

  auto pfx = std::make_shared<InternalPrefix>(base, exponent, name, symbol, description);
  if (!this->addPrefix(pfx))
  {
    Prefix emptyResult;
    return emptyResult;
  }
  return *pfx;
}

Dimension System::createDimension(string::Token symbol, string::Token name, string::Token plural, string::Token description)
{
  if (!name.valid() && !symbol.valid())
  {
    throw std::invalid_argument("A name or symbol must be provided.");
  }
  if (!name.valid() && plural.valid())
  {
    throw std::invalid_argument("You must specify a singular name if you specify a plural form.");
  }
  if (name.valid() && m_dimensions.find(name) != m_dimensions.end())
  {
    throw std::invalid_argument("Duplicate dimension name.");
  }
  if (symbol.valid() && m_dimensions.find(symbol) != m_dimensions.end())
  {
    throw std::invalid_argument("Duplicate dimension symbol.");
  }

  auto dim = std::make_shared<InternalDimension>(name, symbol, description);
  if (!this->addDimension(dim))
  {
    Dimension emptyResult;
    return emptyResult;
  }
  Dimension result({*dim, 1});
  return result;
}

Unit System::createUnit(
  const Dimension& dimension,
  string::Token symbol,
  string::Token name,
  string::Token plural,
  string::Token description)
{
  if (!name.valid() && !symbol.valid())
  {
    throw std::invalid_argument("A name or symbol must be provided.");
  }
  if (!name.valid() && plural.valid())
  {
    throw std::invalid_argument("You must specify a singular name if you specify a plural form.");
  }
  if (name.valid() && m_unitsByName.find(name) != m_unitsByName.end())
  {
    throw std::invalid_argument("Duplicate unit name.");
  }
  if (symbol.valid() && m_unitsByName.find(symbol) != m_unitsByName.end())
  {
    throw std::invalid_argument("Duplicate unit symbol.");
  }

  auto unit = std::make_shared<InternalUnit>(dimension, symbol, name, plural, description);
  if (!this->addUnit(unit))
  {
    Unit badResult;
    return badResult;
  }
  Unit result({*unit, 1});
  return result;
}

bool System::addPrefix(const std::shared_ptr<InternalPrefix>& pfx)
{
  if (!pfx || (!pfx->m_name.valid() && !pfx->m_symbol.valid()))
  {
    return false;
  }

  // Index by both the name(s) and symbol
  if (pfx->m_symbol.valid()) { m_prefixes[pfx->m_symbol] = pfx; }
  if (pfx->m_name.valid()) { m_prefixes[pfx->m_name] = pfx; }
  if (pfx->m_plural.valid()) { m_prefixes[pfx->m_plural] = pfx; }
  for (const auto& alias : pfx->m_symbolAliases)
  {
    m_prefixes[alias] = pfx;
  }

  return true;
}

bool System::addDimension(const std::shared_ptr<InternalDimension>& dim)
{
  if (!dim || (!dim->m_name.valid() && !dim->m_symbol.valid()))
  {
    return false;
  }

  // Mark the system as owning this dimension.
  dim->m_system = this;

  // Index by both the name(s) and symbol
  if (dim->m_symbol.valid()) { m_dimensions[dim->m_symbol] = dim; }
  if (dim->m_name.valid()) { m_dimensions[dim->m_name] = dim; }
  if (dim->m_plural.valid())
  {
    m_dimensions[dim->m_plural] = dim;
  }
  else if (!dim->m_noPlural)
  {
    std::string plural = dim->m_name.data() + "s";
    m_dimensions[plural] = dim;
  }
  for (const auto& alias : dim->m_symbolAliases)
  {
    m_dimensions[alias] = dim;
  }
  for (const auto& alias : dim->m_nameAliases)
  {
    m_dimensions[alias] = dim;
  }

  if (auto compositeDimension = std::dynamic_pointer_cast<CompositeDimension>(dim))
  {
    m_compositeDimensions[compositeDimension->m_basis] = compositeDimension;
  }

  return true;
}

bool System::addUnit(const std::shared_ptr<InternalUnit>& unit)
{
  std::lock_guard<std::mutex> blockSolver(m_p->m_solverGuard);
  if (!unit || (!unit->m_name.valid() && !unit->m_symbol.valid()))
  {
    return false;
  }

  // Take ownership of the unit.
  unit->m_parent = this;
  std::size_t unitIdx = m_units.size();
  m_units.push_back(unit);
  unit->m_index = unitIdx;

  // Index by both the name(s) and symbol(s).
  if (unit->m_symbol.valid()) { m_unitsByName[unit->m_symbol] = unit.get(); }
  if (unit->m_name.valid()) { m_unitsByName[unit->m_name] = unit.get(); }
  if (unit->m_plural.valid())
  {
    m_unitsByName[unit->m_plural] = unit.get();
  }
  else
  {
    std::string plural = unit->m_name.data() + "s";
    m_unitsByName[plural] = unit.get();
  }
  for (const auto& alias : unit->m_symbolAliases)
  {
    m_unitsByName[alias] = unit.get();
  }
  for (const auto& alias : unit->m_nameAliases)
  {
    m_unitsByName[alias] = unit.get();
  }

  // Index by dimension as well.
  m_indexByDimension[unit->m_dimensions].insert(unit.get());
  return true;
}

bool System::addConversion(const std::shared_ptr<Conversion>& conversion)
{
  std::lock_guard<std::mutex> blockSolver(m_p->m_solverGuard);
  if (!conversion)
  {
    return false;
  }

  // Take ownership of the conversion.
  m_conversions.push_back(conversion);

  // Index the conversion by source and target unit.
  for (const auto& entry : conversion->m_source.m_data)
  {
    m_conversionsByUnit[entry.first].insert(std::make_pair(conversion.get(), true));
  }
  for (const auto& entry : conversion->m_target.m_data)
  {
    m_conversionsByUnit[entry.first].insert(std::make_pair(conversion.get(), false));
  }

  return true;
}

Dimension System::dimension(const std::string& dimensionString, bool* success) const
{
  // TODO: We want to support spaces in dimension long names.
  //       This "pre-check" before parsing supports only a single
  //       dimemsion, but accommodates matching names with spaces in them.
  auto it = m_dimensions.find(dimensionString);
  if (it != m_dimensions.end())
  {
    if (success)
    {
      *success = true;
    }
    return Dimension{*it->second, 1};
  }

  // We didn't find an exact match to a long name… parse it as an expression
  grammar::measurement_state state;
  bool ok;
  try
  {
    auto* self = const_cast<System*>(this);
    state.system = self->shared_from_this();
    state.parsing_dimensions = true;
    ok = parseUnits(dimensionString, state);
  }
  catch (std::exception& e)
  {
    ok = false;
  }
  if (success)
  {
    *success = ok;
  }
  return state.dimensions;
}

Unit System::unit(const std::string& unitString, bool* success) const
{
  grammar::measurement_state state;
  try
  {
    auto* self = const_cast<System*>(this);
    state.system = self->shared_from_this();
    bool ok = parseUnits(unitString, state);
    if (success)
    {
      *success = ok;
    }
  }
  catch (std::exception& e)
  {
    if (success)
    {
      *success = false;
    }
  }
  return state.result;
}

Measurement System::measurement(const std::string& text, bool* success)
{
  grammar::measurement_state state;
  try
  {
    auto* self = const_cast<System*>(this);
    state.system = self->shared_from_this();
    state.parsing_measurement = true;
    bool ok = parseUnits(text, state);
    if (success)
    {
      *success = state.have_value && ok;
    }
  }
  catch (std::exception& e)
  {
    if (success)
    {
      *success = false;
    }
  }
  return Measurement(state.value, state.result);
}

std::unique_ptr<Converter> System::convert(const Unit& source, const Unit& target) const
{
#ifdef UNITS_DBG_CONVERT
  std::cout << "Attempt conversion from " << source << " to " << target << "\n";
#endif
  std::unique_ptr<Converter> result;
  // Create a dummy measurement to make life a little easier.
  Measurement lambda(1.0, source);

  // Terminate early if dimensions do not match.
  // TODO: It is possible for dimensions to not match and still
  //       have a conversion in some cases (if the System is
  //       configured with multiple CompositeDimension objects
  //       or a "null" Dimension). We should handle these cases.
  if (lambda.m_units.dimension() != target.dimension())
  {
#ifdef UNITS_DBG_CONVERT
    std::cout
      << "Dimensions mismatch: " << lambda.m_units.dimension()
      << " vs. " << target.dimension() << "\n";
#endif
    return result;
  }

  bool ok = false;
  std::set<std::pair<Conversion*, double>> conversionsAndPowers;
  {
    // Lock the solver (which also prevents m_units and m_conversion from being modified)
    // so we can compute a solution with a consistent mapping between matrix entries
    // and the conversions/units they represent.
    std::lock_guard<std::mutex> blockSolver(m_p->m_solverGuard);
    try
    {
      // Populate the right-hand-side vector, uu:
      Eigen::VectorXd uu = Eigen::VectorXd::Zero(static_cast<Eigen::Index>(m_units.size()));
      for (const auto& unitAndPower : target.m_data)
      {
        uu(unitAndPower.first->m_index) += unitAndPower.second;
      }
      for (const auto& unitAndPower : lambda.m_units.m_data)
      {
        uu(unitAndPower.first->m_index) -= unitAndPower.second;
      }

      m_p->updateFactorization();
      Eigen::VectorXd ss = m_p->m_solver.solve(uu);
      ok = m_p->m_solver.info() == Eigen::Success;
      if (!ok)
      {
        return result;
      }
#ifdef UNITS_DBG_CONVERT
      std::cout << "  Conversion rules that may apply:\n";
#endif
      for (Eigen::Index ii = 0; ii < ss.rows(); ++ii)
      {
        if (std::abs(ss(ii)) > 1e-10)
        {
          conversionsAndPowers.insert({m_conversions[ii].get(), std::round(ss(ii)*100.0)/100.0});
#ifdef UNITS_DBG_CONVERT
          std::cout
            << "    " << m_conversions[ii]->m_source
            << " → " << m_conversions[ii]->m_target
            << ": " << std::round(ss(ii)*100.0)/100.0
            << "\n";
#endif // UNITS_DBG_CONVERT
        }
      }
    }
    catch (std::exception& e)
    {
      std::cerr << "Oopsie: " << e.what() << "\n";
      ok = false;
      return result;
    }
  }

  result = std::make_unique<Converter>(source, target);
  auto canonicalTarget = target.canonical();

  OffsetRule::Context context;
  context.absolute = lambda.m_units.simple();

  std::vector<std::tuple<Conversion*, double, bool>> converterPlan; // Output sequence of converters to invoke.
  while (!conversionsAndPowers.empty())
  {
    bool foundMatch = false;
    auto cpit = conversionsAndPowers.begin();
    for (; cpit != conversionsAndPowers.end(); ++cpit)
    {
      double sourceFactor = lambda.m_units.factor(cpit->first->m_source);
      double targetFactor = lambda.m_units.factor(cpit->first->m_target);

      if (sourceFactor != 0.)
      {
        context.forward = true;
        context.exponent = sourceFactor;
        // Apply conversion forward
        cpit->first->apply(lambda, context);
        result->m_transforms.push_back(cpit->first->evaluator(context));
        foundMatch = true;
#ifdef UNITS_DBG_CONVERT
        std::cout
          << "      Apply " << cpit->first->m_source << " → " << cpit->first->m_target
          << " exp " << sourceFactor << " to get " << lambda.m_units << "\n";
#endif
        break;
      }
      else if (targetFactor != 0.)
      {
        context.forward = false;
        context.exponent = targetFactor;
        // Apply conversion backward
        cpit->first->apply(lambda, context);
        result->m_transforms.push_back(cpit->first->evaluator(context));
        foundMatch = true;
#ifdef UNITS_DBG_CONVERT
        std::cout
          << "      Apply " << cpit->first->m_target << " → " << cpit->first->m_source
          << " exp " << targetFactor << " to get " << lambda.m_units << "\n";
#endif
        break;
      }
    }
    if (foundMatch)
    {
      conversionsAndPowers.erase(cpit);
    }
    else
    {
      std::cerr << "Unable to make forward progress.\n";
      result = nullptr;
      return result;
    }
    if (lambda.m_units.canonical() == canonicalTarget)
    {
      if (!conversionsAndPowers.empty())
      {
        std::cerr << "Warning: " << conversionsAndPowers.size() << " conversions unneeded.\n";
      }
      break;
    }
  }

  if (canonicalTarget != target)
  {
    // Undo canonical conversion so we report in desired units.
    for (const auto& entry : target.m_powers)
    {
      lambda.m_value *= std::pow(entry.first, -entry.second);
      lambda.m_units.m_powers[entry.first] += entry.second;
      result->m_scaling *= std::pow(entry.first, -entry.second);
    }
  }
  // If the result has prefixes that were not requested on the output
  if (lambda.m_units.m_powers != target.m_powers)
  {
    auto rit = lambda.m_units.m_powers.begin();
    auto next = rit;
    for (; next != lambda.m_units.m_powers.end();)
    {
      rit = next;
      ++next;
      auto it = target.m_powers.find(rit->first);
      double desired = (it == target.m_powers.end() ? 0.0 : it->second);
      if (desired != rit->second)
      {
        lambda.m_value *= std::pow(rit->first, rit->second - desired);
        result->m_scaling *= std::pow(rit->first, rit->second - desired);
        rit->second = desired;
        if (rit->second == 0.0)
        {
          lambda.m_units.m_powers.erase(rit);
        }
      }
    }
  }

  return result;
}

Measurement System::convert(const Measurement& source, const Unit& target, bool* success) const
{
  Measurement result;
  auto converter = this->convert(source.m_units, target);
  if (!converter)
  {
    if (success) { *success = false; }
    return result;
  }
  result = converter->transform(source);
  if (success) { *success = true; }
  return result;
}

std::vector<Unit> System::compatibleUnits(
  const Unit& unit,
  const CompatibleUnitOptions& options) const
{
  if (options.m_useActiveContext)
  {
    auto it = m_unitContexts.find(m_activeUnitContext);
    if (it != m_unitContexts.end())
    {
      return it->second->suggestedUnits(unit, options);
    }
  }

  std::vector<Unit> result;
  auto it = m_indexByDimension.find(unit.dimension());
  if (it == m_indexByDimension.end())
  {
    auto compIt = m_compositeDimensions.find(unit.dimension());
    if (compIt == m_compositeDimensions.end())
    {
      return result;
    }
    it = m_indexByDimension.find(Dimension({*compIt->second, 1}));
    if (it == m_indexByDimension.end())
    {
      return result;
    }
  }
  for (const auto& entry : it->second)
  {
    result.push_back(Unit{*entry, 1});
  }
  std::sort(result.begin(), result.end(), Unit::compareByNameAscending);
  return result;
}

units_CLOSE_NAMESPACE
