#include "units/PreferredUnits.h"

#include "units/CompatibleUnitOptions.h"
#include "units/System.h"
#include "units/Unit.h"

units_BEGIN_NAMESPACE

std::shared_ptr<PreferredUnits> PreferredUnits::create(System* system, bool empty)
{
  auto result = std::make_shared<PreferredUnits>();
  result->m_system = system;
  if (!system || empty)
  {
    return result;
  }
  // By default, populate with alphabetized units for each dimension
  // in the system.
  CompatibleUnitOptions options;
  options.m_useActiveContext = false;
  options.m_inputUnitPriority = -1;
  for (const auto& entry : system->m_indexByDimension)
  {
    if (entry.second.empty()) { continue; }
    Unit exemplar{**entry.second.begin(), 1.0};
    auto compat = system->compatibleUnits(exemplar, options);
    for (const auto& unit : compat)
    {
      result->m_unitsByDimension[entry.first].push_back(UnitAndLabel{unit, unit.name()});
    }
  }
  return result;
}

std::shared_ptr<PreferredUnits> PreferredUnits::copy()
{
  auto result = PreferredUnits::create(m_system, true);
  result->m_userEditable = m_userEditable;
  result->m_unitsByDimension = m_unitsByDimension;
  return result;
}

std::vector<Unit> PreferredUnits::suggestedUnits(const Unit& exemplar, CompatibleUnitOptions options)
{
  std::vector<Unit> result;
  auto it = m_unitsByDimension.find(exemplar.dimension());
  if (it != m_unitsByDimension.end())
  {
    for (const auto& entry : it->second)
    {
      result.push_back(entry.m_unit);
    }
  }
  if (options.m_inputUnitPriority == -1)
  {
    // Do not force exemplar to be in output.
    return result;
  }

  std::size_t idx = 0;
  bool found = false;
  for (const auto& unit : result)
  {
    if (unit == exemplar)
    {
      found = true;
      break;
    }
    ++idx;
  }
  if (options.m_inputUnitPriority == -2)
  {
    // Force the exemplar to be on the list (at the bottom) if it is not already present.
    if (!found)
    {
      result.push_back(exemplar);
    }
  }
  else if (options.m_inputUnitPriority >= 0)
  {
    if (found && idx == options.m_inputUnitPriority)
    {
      return result;
    }
    if (found)
    {
      // Remove the exemplar from where it was.
      result.erase(result.begin() + idx);
    }
    // Adjust the location we'll insert the exemplar to a valid index.
    if (idx > result.size())
    {
      idx = result.size();
    }
    // Insert the exemplar.
    result.insert(result.begin() + idx, exemplar);
  }
  return result;
}

// std::vector<Unit> PreferredUnits::suggestedUnits(const Dimension& dimension);

units_CLOSE_NAMESPACE
