#ifndef units_CompatibleUnitOptions
#define units_CompatibleUnitOptions
/*!\file CompatibleUnitOptions.h */

#include "units/Exports.h"
#include "units/Options.h"

units_BEGIN_NAMESPACE

/// Specify how compatible units should be ordered.
struct UNITS_EXPORT CompatibleUnitOptions
{
  CompatibleUnitOptions() = default;

  /// If true, use the active unit context's suggestions.
  ///
  /// Otherwise, generate a list of all compatible primary units
  /// with the same dimension as the input units.
  bool m_useActiveContext{ true };

  /// Control where the input unit is placed in the output vector.
  ///
  /// + If -1, the input unit is not forced into the output vector.
  /// + If -2, the input unit is appended to the end of the output vector
  ///   if it does not appear elsewhere.
  /// + If non-negative, the input unit is placed at the given offset
  ///   into the vector (displacing any units beneath it).
  ///   If the integer is larger than the number of suggested units,
  ///   then the input unit is placed at the bottom of the output vector.
  ///
  /// The default is -2.
  int m_inputUnitPriority{ -2 };
};

units_CLOSE_NAMESPACE

#endif // units_CompatibleUnitOptions
