#include "units/Measurement.h"

#include <iostream>

units_BEGIN_NAMESPACE

Measurement::Measurement(double value, Unit units)
  : m_value(value)
  , m_units(units)
{
}

void Measurement::dump() const
{
  std::cout << *this << "\n";
}

units_CLOSE_NAMESPACE
