#ifndef units_Unit_h
#define units_Unit_h

#include "units/Dimension.h"

units_BEGIN_NAMESPACE

struct InternalDimension;
struct Prefix;
struct System;

/// Any simple (non-composite) unit.
struct UNITS_EXPORT InternalUnit : Entity
{
  using Exponents = std::map<InternalUnit*, double>;

  InternalUnit() = default;

  /// Do not allow copying of InternalUnit instances.
  InternalUnit(const InternalUnit&) = delete;

  InternalUnit(
    const Dimension& dimensions,
    units::string::Token symbol,
    units::string::Token name,
    units::string::Token description);

  InternalUnit(
    const Dimension& dimensions,
    units::string::Token symbol,
    units::string::Token name,
    units::string::Token plural,
    units::string::Token description);

  /// Force InternalUnit to be polymorphic.
  virtual ~InternalUnit() = default;

  /// Do not allow copying of InternalUnit instances.
  void operator = (const InternalUnit&) = delete;

  static Dimension dimensionsFromBasis(const InternalUnit::Exponents& basis);

  System* m_parent{ nullptr };
  Dimension m_dimensions;
  std::size_t m_index{ 0 };
};

/// A user-facing unit.
///
/// Instances of this struct may be copied, assigned, multiplied,
/// divided, raised to powers, etc.
///
/// The units and their exponents are stored in m_data
/// while any prefixed scalar multipliers are stored in m_powers.
/// Normally, m_powers will have no entries, an entry for 2, or
/// an entry for 10. Note that m_powers is not intended to hold
/// a scalar quanitity; rather it is a multiplier used to track
/// how Prefixes raised to powers are attached to the unit.
struct UNITS_EXPORT Unit
{
  using Powers = std::map<int, double>;
#if 0
  /// Hold information on a per-InternalUnit basis.
  struct PerUnit
  {
    /// What prefix(es) have been applied to the corresponding unit.
    Powers m_powers;
    /// The power to which the corresponding unit has been raised.
    double m_exponent;
    /// Whether the corresponding unit was provided as an absolute value
    /// or a relative value (i.e., a difference).
    ///
    /// This is used to factor out offsets in conversions with an OffsetRule.
    bool m_absolute{ false };
  };
  using Basis = std::map<InternalUnit*, PerUnit>;
#endif // 0

  Unit() = default;
  Unit(InternalUnit& unit, double exponent = 1.0)
  {
    m_data[&unit] = exponent;
  }
  Unit(const InternalUnit::Exponents& data)
    : m_data(data)
  {
  }

  Unit(const Unit& other)
    : m_data(other.m_data)
    , m_powers(other.m_powers)
  {
  }

  /// Return this unit raised to the given \a exponent.
  Unit pow(double exponent) const;

  /// Return the unit system from whence this unit came.
  System* system() const;

  /// Generate a name that can be parsed back to this unit.
  std::string name() const;

  /// Return the dimension(s) in which this unit has extents.
  Dimension dimension() const;

  /// Return whether the unit is "empty" (dimensionless) or not.
  bool dimensionless() const;

  /// Return whether the unit is "simple" or not.
  ///
  /// A unit is simple when it is not composite and not raised
  /// to any power (i.e., m_data contains a single entry and that
  /// entry's exponent is exactly 1.0).
  ///
  /// This is currently used by OffsetRule instances to determine
  /// whether a measurement's value is absolute or relative.
  bool simple() const;

  /// Return true if the \a other unit overlaps this unit.
  ///
  /// Units "overlap" when they share references to one or more common InternalUnit.
  bool overlaps(const Unit& other) const;

  /// If \a other is a subset of InternalUnits held by \a this unit
  /// and every power in \a other is a factor of the factors held
  /// by \a this unit, return smallest factor.
  ///
  /// This method returns 0 if any of the following hold:
  /// + \a other is not a subset of \a this; or
  /// + \a other has any exponent greater than those in \a this unit; or
  /// + \a other and \a this have opposite signs for any strict subset
  ///   of their shared exponents.
  ///
  /// If all the shared exponents have opposite signs, this method
  /// will return a negative number (indicating the inverse power of
  /// \a other present in \a this unit).
  double factor(const Unit& other) const;

  /// Return a "canonical" form of this unit (with no prefix multipliers).
  Unit canonical() const;

  Unit& operator *= (const Unit& other);
  Unit& operator /= (const Unit& other);

  /// For debugging, print this Unit to the console.
  void dump() const;

  /// This function may be used to sort Unit instances
  /// alphabetically by their names.
  static bool compareByNameAscending(const Unit& aa, const Unit& bb);

  /// A collection of units mapped to their exponents.
  InternalUnit::Exponents m_data;
  /// A collection of numeric bases mapped to their exponents.
  Powers m_powers;
};

UNITS_EXPORT Unit operator * (const Prefix& pp, const Unit& uu);
UNITS_EXPORT Unit operator * (const Unit& aa, const Unit& bb);
UNITS_EXPORT Unit operator / (const Unit& aa, const Unit& bb);
UNITS_EXPORT bool operator == (const Unit& aa, const Unit& bb);
UNITS_EXPORT bool operator != (const Unit& aa, const Unit& bb);
UNITS_EXPORT bool operator < (const Unit& aa, const Unit& bb);

/// Print a unit's composition to the given stream \a str.
template<typename Stream>
Stream& operator << (Stream& str, const Unit& unit)
{
  bool once = false;
  if (!unit.m_powers.empty())
  {
    for (const auto& basePower : unit.m_powers)
    {
      if (!once)
      {
        once = true;
      }
      else
      {
        str << "·";
      }
      if (basePower.second != 1.0)
      {
        str << basePower.first << "^" << basePower.second;
      }
      else
      {
        str << basePower.first;
      }
    }
  }
  if (unit.dimensionless())
  {
    if (!once) { str << "·"; }
    // An empty set of exponents means the data is dimensionless.
    str << "∅";
    return str;
  }
  for (const auto& entry : unit.m_data)
  {
    if (!entry.first) { continue; }
    {
      if (once)
      {
        str << "·";
      }
      else
      {
        once = true;
      }
      if (entry.first->m_symbol.valid())
      {
        str << entry.first->m_symbol.data();
      }
      else if (entry.first->m_plural.valid())
      {
        str << entry.first->m_plural.data();
      }
      else
      {
        str << entry.first->m_name.data();
      }
      if (entry.second != 1.0)
      {
        str << "^" << entry.second;
      }
    }
  }
  return str;
}

units_CLOSE_NAMESPACE

#endif // units_Unit_h
