#ifndef units_Dimension_h
#define units_Dimension_h

#include "units/Entity.h"

#include <map>

units_BEGIN_NAMESPACE

struct System;

/// One of the seven or so base dimensions.
///
/// This class is not intended for manipulation by users; it is managed
/// (via shared pointer) by a units::System and should not be copied or
/// modified.
struct UNITS_EXPORT InternalDimension : Entity
{
  /// A container for holding composite dimensions and their powers (exponents).
  using Exponents = std::map<InternalDimension*, double>;

  /// Constructors.
  InternalDimension() = default;
  InternalDimension(const InternalDimension&) = delete; // Base dimensions are managed by a System.
  InternalDimension(units::string::Token symbol, units::string::Token name, units::string::Token description);
  InternalDimension(units::string::Token symbol, units::string::Token name, units::string::Token plural, units::string::Token description);
  /// Make InternalDimension polymorphic so it can be dynamically cast.
  virtual ~InternalDimension() = default;

  /// Return the dimension's owning unit system, if any exists.
  System* system() const { return m_system; }

  void operator = (const InternalDimension&) = delete; // Base dimensions are managed by a System.

  /// The owning unit system. This is set by the System when the dimension is added.
  System* m_system{ nullptr };
};

/// A derived dimension that can be expressed as a combination of base dimensions.
///
/// This class is not intended for manipulation by users; it is managed
/// (via shared pointer) by a units::System and should not be copied or
/// modified.
struct UNITS_EXPORT CompositeDimension : InternalDimension
{
  CompositeDimension() = default;
  CompositeDimension(const CompositeDimension& other);
  CompositeDimension(units::string::Token symbol, units::string::Token name, units::string::Token description);
  CompositeDimension(
    units::string::Token symbol, units::string::Token name, const Exponents& basis, units::string::Token description);
  CompositeDimension(
    units::string::Token symbol, units::string::Token name, const CompositeDimension& basis, units::string::Token description);
  CompositeDimension(
    units::string::Token symbol, units::string::Token name, units::string::Token plural, units::string::Token description);
  CompositeDimension(
    units::string::Token symbol,
    units::string::Token name,
    units::string::Token plural,
    const Exponents& basis,
    units::string::Token description);
  CompositeDimension(
    units::string::Token symbol,
    units::string::Token name,
    units::string::Token plural,
    const CompositeDimension& basis,
    units::string::Token description);
  ~CompositeDimension() override = default;
  CompositeDimension& operator = (const CompositeDimension& other);

  Exponents m_basis;
};

/// A dimension that can be manipulated by the user.
///
/// Instances of this object can be copied, assigned, multiplied, divided, etc.
/// Internally, their `m_data` member will always refer to "managed" instances
/// of InternalDimension or CompositeDimension.
struct UNITS_EXPORT Dimension
{
  Dimension() = default;
  Dimension(const InternalDimension& dim, double exponent = 1.0)
  {
    m_data[const_cast<InternalDimension*>(&dim)] = exponent;
  }

  Dimension(const InternalDimension::Exponents& data)
    : m_data(data)
  {
  }

  Dimension(const Dimension& other)
    : m_data(other.m_data)
  {
  }

  Dimension pow(double exponent) const;

  System* system() const;
  units::string::Token name() const;

  /// Return an HTML string with the dimension.
  ///
  /// The string will be a UTF-8 HTML string that uses `<sup>` tags
  /// for exponents and dimension symbols (rather than names) where
  /// they exist.
  /// For example, a force might be reported as `M·L·T<sup>-2</sup>`
  /// with \a principleDimensionsOnly true or as `force`
  /// with \a principleDimensionsOnly false.
  ///
  /// If \a principleDimensionsOnly is true (the default), only
  /// report principle (non-composite) dimensions.
  std::string html(bool principleDimensionsOnly = true) const;

  /// Print the dimension to the console for debugging purposes.
  void dump() const;

  InternalDimension::Exponents m_data;
};

UNITS_EXPORT Dimension operator * (const Dimension& aa, const Dimension& bb);
UNITS_EXPORT Dimension operator / (const Dimension& aa, const Dimension& bb);
UNITS_EXPORT bool operator == (const Dimension& aa, const Dimension& bb);
UNITS_EXPORT bool operator != (const Dimension& aa, const Dimension& bb);
UNITS_EXPORT bool operator < (const Dimension& aa, const Dimension& bb);

/// Print a dimension's composition to the given stream \a str.
template<typename Stream>
Stream& operator << (Stream& str, const Dimension& dim)
{
  if (dim.m_data.empty())
  {
    str << "∅";
    return str;
  }
  bool once = false;
  for (const auto& entry : dim.m_data)
  {
    if (!entry.first) { continue; }
    if (const auto* dd = dynamic_cast<const CompositeDimension*>(entry.first))
    {
      if (dd->m_symbol.valid() && !dd->m_symbol.data().empty())
      {
        if (once)
        {
          str << "·";
        }
        else
        {
          once = true;
        }
        str << dd->m_symbol.data();
        if (entry.second != 1.0)
        {
          str << "^" << entry.second;
        }
      }
      else
      {
        for (const auto& innerEntry : dd->m_basis)
        {
          if (!once)
          {
            once = true;
            str << innerEntry.first->m_symbol.data();
          }
          else
          {
            str << "·" << innerEntry.first->m_symbol.data();
          }
          double pwr = innerEntry.second * entry.second;
          if (pwr != 1.0)
          {
            str << "^" << pwr;
          }
        }
      }
    }
    else
    {
      if (once)
      {
        str << "·";
      }
      else
      {
        once = true;
      }
      str << entry.first->m_symbol.data();
      if (entry.second != 1.0)
      {
        str << "^" << entry.second;
      }
    }
  }
  return str;
}

units_CLOSE_NAMESPACE

#endif // units_Dimension_h
