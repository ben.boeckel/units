# We require units' version information to be included the CMake Project
# declaration in order to properly include units versions when using units as an
# SDK. We therefore do not have access to the variable ${PROJECT_SOURCE_DIR}
# at the time unitsVersion.cmake is read. We therefore use
# ${CMAKE_CURRENT_SOURCE_DIR}, since unitsVersion.cmake is called at the top
# level of units's build. We also guard against subsequent calls to
# unitsVersion.cmake elsewhere in the build setup where
# ${CMAKE_CURRENT_SOURCE_DIR} may no longer be set to the top level directory.

if (NOT DEFINED units_VERSION)

  file(STRINGS ${CMAKE_CURRENT_SOURCE_DIR}/version.txt version_string )

  string(REGEX MATCH "([0-9]+)\\.([0-9]+)\\.([0-9]+)[-]*(.*)"
    version_matches "${version_string}")

  set(units_VERSION_MAJOR ${CMAKE_MATCH_1})
  set(units_VERSION_MINOR ${CMAKE_MATCH_2})
  set(units_VERSION "${units_VERSION_MAJOR}.${units_VERSION_MINOR}")
  if (DEFINED CMAKE_MATCH_3)
    set(units_VERSION_PATCH ${CMAKE_MATCH_3})
    set(units_VERSION "${units_VERSION}.${units_VERSION_PATCH}")
  else()
    set(units_VERSION_PATCH 0)
  endif()
  # To be thorough, we should split the label into "-prerelease+metadata"
  # and, if prerelease is specified, use it in determining precedence
  # according to semantic versioning rules at http://semver.org/ .
  # For now, just make the information available as a label:
  if (DEFINED CMAKE_MATCH_4)
    set(units_VERSION_LABEL "${CMAKE_MATCH_4}")
  endif()

  # Remove leading zeros from units version so that when using zero-padded month
  # numbers as the minor version number we do not have problems converting to
  # a base-10 integer.
  set(units_VERSION_MINOR_STRING ${units_VERSION_MINOR})
  string(REGEX REPLACE "^0" "" units_VERSION_MINOR_INT ${units_VERSION_MINOR})
endif()
