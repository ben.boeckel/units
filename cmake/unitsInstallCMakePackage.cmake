if (NOT (DEFINED units_cmake_dir AND
         DEFINED units_cmake_build_dir AND
         DEFINED units_cmake_destination))
  message(FATAL_ERROR
    "unitsInstallCMakePackage is missing input variables.")
endif ()

set(prefix_file "${units_cmake_build_dir}/units-prefix.cmake")
file(WRITE "${prefix_file}"
  "set(_units_import_prefix \"\${CMAKE_CURRENT_LIST_DIR}\")\n")
set(destination "${units_cmake_destination}")
while (destination)
  get_filename_component(destination "${destination}" DIRECTORY)
  file(APPEND "${prefix_file}"
    "get_filename_component(_units_import_prefix \"\${_units_import_prefix}\" DIRECTORY)\n")
endwhile ()

configure_file(
  "${units_cmake_dir}/unitsConfig.cmake.in"
  "${units_cmake_build_dir}/unitsConfig.cmake"
  @ONLY)
include(CMakePackageConfigHelpers)
write_basic_package_version_file("${units_cmake_build_dir}/unitsConfigVersion.cmake"
  VERSION "${units_VERSION}"
  COMPATIBILITY AnyNewerVersion)

# For convenience, a package is written to the top of the build tree. At some
# point, this should probably be deprecated and warn when it is used.
file(GENERATE
  OUTPUT  "${CMAKE_BINARY_DIR}/unitsConfig.cmake"
  CONTENT "include(\"${units_cmake_build_dir}/unitsConfig.cmake\")\n")
configure_file(
  "${units_cmake_build_dir}/unitsConfigVersion.cmake"
  "${CMAKE_BINARY_DIR}/unitsConfigVersion.cmake"
  COPYONLY)

set(units_cmake_module_files
  unitsMacros.cmake
)

set(units_cmake_files_to_install
  "${prefix_file}")

foreach (units_cmake_module_file IN LISTS units_cmake_module_files)
  configure_file(
    "${units_cmake_dir}/${units_cmake_module_file}"
    "${units_cmake_build_dir}/${units_cmake_module_file}"
    COPYONLY)
  list(APPEND units_cmake_files_to_install
    "${units_cmake_module_file}")
endforeach ()

include(unitsInstallCMakePackageHelpers)
if (NOT units_RELOCATABLE_INSTALL)
  list(APPEND units_cmake_files_to_install
    "${units_cmake_build_dir}/units-find-package-helpers.cmake")
endif ()

foreach (units_cmake_file IN LISTS units_cmake_files_to_install)
  if (IS_ABSOLUTE "${units_cmake_file}")
    file(RELATIVE_PATH units_cmake_subdir_root "${units_cmake_build_dir}" "${units_cmake_file}")
    get_filename_component(units_cmake_subdir "${units_cmake_subdir_root}" DIRECTORY)
    set(units_cmake_original_file "${units_cmake_file}")
  else ()
    get_filename_component(units_cmake_subdir "${units_cmake_file}" DIRECTORY)
    set(units_cmake_original_file "${units_cmake_dir}/${units_cmake_file}")
  endif ()
  install(
    FILES       "${units_cmake_original_file}"
    DESTINATION "${units_cmake_destination}/${units_cmake_subdir}"
    COMPONENT   "development")
endforeach ()

install(
  FILES       "${units_cmake_build_dir}/unitsConfig.cmake"
              "${units_cmake_build_dir}/unitsConfigVersion.cmake"
  DESTINATION "${units_cmake_destination}"
  COMPONENT   "development")
