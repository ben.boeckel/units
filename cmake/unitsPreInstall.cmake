# Save compile-time options for use by other packages
configure_file(
  ${PROJECT_SOURCE_DIR}/cmake/Options.h.in
  ${PROJECT_BINARY_DIR}/${PROJECT_NAME}/Options.h
  @ONLY)

install (FILES ${PROJECT_BINARY_DIR}/${PROJECT_NAME}/Options.h
  DESTINATION include/${PROJECT_NAME}/${units_VERSION}/units)
